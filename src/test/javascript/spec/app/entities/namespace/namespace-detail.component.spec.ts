/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { K8SOrchestratorTestModule } from '../../../test.module';
import { NamespaceDetailComponent } from 'app/entities/namespace/namespace-detail.component';
import { Namespace } from 'app/shared/model/namespace.model';

describe('Component Tests', () => {
  describe('Namespace Management Detail Component', () => {
    let comp: NamespaceDetailComponent;
    let fixture: ComponentFixture<NamespaceDetailComponent>;
    const route = ({ data: of({ namespace: new Namespace(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [K8SOrchestratorTestModule],
        declarations: [NamespaceDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(NamespaceDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(NamespaceDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.namespace).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
