/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { K8SOrchestratorTestModule } from '../../../test.module';
import { NamespaceComponent } from 'app/entities/namespace/namespace.component';
import { NamespaceService } from 'app/entities/namespace/namespace.service';
import { Namespace } from 'app/shared/model/namespace.model';

describe('Component Tests', () => {
  describe('Namespace Management Component', () => {
    let comp: NamespaceComponent;
    let fixture: ComponentFixture<NamespaceComponent>;
    let service: NamespaceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [K8SOrchestratorTestModule],
        declarations: [NamespaceComponent],
        providers: []
      })
        .overrideTemplate(NamespaceComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(NamespaceComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(NamespaceService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Namespace(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.namespaces[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
