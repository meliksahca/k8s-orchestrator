/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { HcmEventManager } from 'app/framework/public_api';

import { K8SOrchestratorTestModule } from '../../../test.module';
import { NamespaceDeleteDialogComponent } from 'app/entities/namespace/namespace-delete-dialog.component';
import { NamespaceService } from 'app/entities/namespace/namespace.service';

describe('Component Tests', () => {
  describe('Namespace Management Delete Component', () => {
    let comp: NamespaceDeleteDialogComponent;
    let fixture: ComponentFixture<NamespaceDeleteDialogComponent>;
    let service: NamespaceService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [K8SOrchestratorTestModule],
        declarations: [NamespaceDeleteDialogComponent]
      })
        .overrideTemplate(NamespaceDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(NamespaceDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(NamespaceService);
      mockEventManager = fixture.debugElement.injector.get(HcmEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
