/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { K8SOrchestratorTestModule } from '../../../test.module';
import { NamespaceUpdateComponent } from 'app/entities/namespace/namespace-update.component';
import { NamespaceService } from 'app/entities/namespace/namespace.service';
import { Namespace } from 'app/shared/model/namespace.model';

describe('Component Tests', () => {
  describe('Namespace Management Update Component', () => {
    let comp: NamespaceUpdateComponent;
    let fixture: ComponentFixture<NamespaceUpdateComponent>;
    let service: NamespaceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [K8SOrchestratorTestModule],
        declarations: [NamespaceUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(NamespaceUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(NamespaceUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(NamespaceService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Namespace(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Namespace();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
