/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { K8SOrchestratorTestModule } from '../../../test.module';
import { DeploymentComponent } from 'app/entities/deployment/deployment.component';
import { DeploymentService } from 'app/entities/deployment/deployment.service';
import { Deployment } from 'app/shared/model/deployment.model';

describe('Component Tests', () => {
  describe('Deployment Management Component', () => {
    let comp: DeploymentComponent;
    let fixture: ComponentFixture<DeploymentComponent>;
    let service: DeploymentService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [K8SOrchestratorTestModule],
        declarations: [DeploymentComponent],
        providers: []
      })
        .overrideTemplate(DeploymentComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DeploymentComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DeploymentService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Deployment(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.deployments[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
