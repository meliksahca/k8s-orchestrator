import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { of, throwError } from 'rxjs';

import { K8SOrchestratorTestModule } from '../../../test.module';
import { HcmMetricsMonitoringComponent } from 'app/admin/metrics/metrics.component';
import { HcmMetricsService } from 'app/admin/metrics/metrics.service';

describe('Component Tests', () => {
  describe('HcmMetricsMonitoringComponent', () => {
    let comp: HcmMetricsMonitoringComponent;
    let fixture: ComponentFixture<HcmMetricsMonitoringComponent>;
    let service: HcmMetricsService;

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [K8SOrchestratorTestModule],
        declarations: [HcmMetricsMonitoringComponent]
      })
        .overrideTemplate(HcmMetricsMonitoringComponent, '')
        .compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(HcmMetricsMonitoringComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HcmMetricsService);
    });

    describe('refresh', () => {
      it('should call refresh on init', () => {
        // GIVEN
        const response = {
          timers: {
            service: 'test',
            unrelatedKey: 'test'
          },
          gauges: {
            'jcache.statistics': {
              value: 2
            },
            unrelatedKey: 'test'
          }
        };
        spyOn(service, 'getMetrics').and.returnValue(of(response));

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(service.getMetrics).toHaveBeenCalled();
      });
    });
  });
});
