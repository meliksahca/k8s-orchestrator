import { SpyObject } from './spyobject';
import { HcmAlertService, HcmAlert } from 'app/framework/public_api';

export class MockAlertService extends SpyObject {
  constructor() {
    super(HcmAlertService);
  }
  addAlert(alertOptions: HcmAlert) {
    return alertOptions;
  }
}
