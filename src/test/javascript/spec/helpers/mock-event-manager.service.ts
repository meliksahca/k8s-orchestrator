import { SpyObject } from './spyobject';
import { HcmEventManager } from 'app/framework/public_api';
import Spy = jasmine.Spy;

export class MockEventManager extends SpyObject {
  broadcastSpy: Spy;

  constructor() {
    super(HcmEventManager);
    this.broadcastSpy = this.spy('broadcast').andReturn(this);
  }
}
