package com.hazelcast.meliksahsimsek.async;


import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.util.concurrent.Callable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

public class ExceptionHandlingAsyncTaskExecutorTest {

    private static final RuntimeException exception = new RuntimeException("Eek");
    private static final int testResult = 42;

    private boolean done;
    private Exception handled;
    private MockAsyncTaskExecutor task;
    private ExceptionHandlingAsyncTaskExecutor executor;

    @BeforeEach
    public void setup() {
        done = false;
        handled = null;
        task = spy(new MockAsyncTaskExecutor());
        executor = new TestExceptionHandlingAsyncTaskExecutor(task);
    }

    @Test
    public void testInitializingExecutor() {
        task = spy(new MockAsyncInitializingTaskExecutor());
        executor = new TestExceptionHandlingAsyncTaskExecutor(task);
        Throwable caught = catchThrowable(() -> {
            executor.afterPropertiesSet();
            verify(task).afterPropertiesSet();
        });
        assertThat(caught).isNull();
    }

    @Test
    public void testNonInitializingExecutor() {
        Throwable caught = catchThrowable(() -> {
            executor.afterPropertiesSet();
            verify(task, never()).afterPropertiesSet();
        });
        assertThat(caught).isNull();
    }

    @Test
    public void testDisposableExecutor() {
        task = spy(new MockAsyncDisposableTaskExecutor());
        executor = new TestExceptionHandlingAsyncTaskExecutor(task);
        Throwable caught = catchThrowable(() -> {
            executor.destroy();
            verify(task).destroy();
        });
        assertThat(caught).isNull();
    }

    @Test
    public void testNonDisposableExecutor() {
        Throwable caught = catchThrowable(() -> {
            executor.destroy();
            verify(task, never()).destroy();
        });
        assertThat(caught).isNull();
    }

    private class TestExceptionHandlingAsyncTaskExecutor
            extends ExceptionHandlingAsyncTaskExecutor {

        TestExceptionHandlingAsyncTaskExecutor(AsyncTaskExecutor executor) {
            super(executor);
        }

        @Override
        protected void handle(Exception exception) {
            synchronized (executor) {
                handled = exception;
                super.handle(exception);
                executor.notifyAll();
            }
        }
    }

    private class MockRunnableWithoutException implements Runnable {

        @Override
        public void run() {
            synchronized (executor) {
                done = true;
                executor.notifyAll();
            }
        }
    }

    private class MockRunnableWithException implements Runnable {

        @Override
        public void run() {
            synchronized (executor) {
                done = true;
                throw exception;
            }
        }
    }

    private class MockCallableWithoutException implements Callable<Integer> {

        @Override
        public Integer call() {
            done = true;
            return testResult;
        }
    }

    private class MockCallableWithException implements Callable<Integer> {

        @Override
        public Integer call() {
            done = true;
            throw exception;
        }
    }

    @SuppressWarnings("serial")
    private class MockAsyncTaskExecutor extends SimpleAsyncTaskExecutor {

        public void afterPropertiesSet() {
        }

        public void destroy() {
        }
    }

    @SuppressWarnings("serial")
    private class MockAsyncInitializingTaskExecutor extends MockAsyncTaskExecutor
            implements InitializingBean {
    }

    @SuppressWarnings("serial")
    private class MockAsyncDisposableTaskExecutor extends MockAsyncTaskExecutor
            implements DisposableBean {
    }
}
