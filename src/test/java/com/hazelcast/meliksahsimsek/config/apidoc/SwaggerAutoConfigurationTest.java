package com.hazelcast.meliksahsimsek.config.apidoc;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.hazelcast.meliksahsimsek.config.ApplicationProperties;
import com.hazelcast.meliksahsimsek.config.ApplicationProperties.Swagger;
import com.hazelcast.meliksahsimsek.config.apidoc.customizer.ApplicationSwaggerCustomizer;
import com.hazelcast.meliksahsimsek.config.apidoc.customizer.SwaggerCustomizer;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import javax.annotation.Nullable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spring.web.plugins.ApiSelectorBuilder;
import springfox.documentation.spring.web.plugins.Docket;

public class SwaggerAutoConfigurationTest {

    private Swagger properties;
    private SwaggerAutoConfiguration config;
    private ApiSelectorBuilder builder;

    @Captor
    private ArgumentCaptor<ApiInfo> infoCaptor;

    @Captor
    private ArgumentCaptor<Predicate<String>> pathsCaptor;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);

        final ApplicationProperties applicationProperties = new ApplicationProperties();
        properties = applicationProperties.getSwagger();
        properties.setHost("test.host.org");
        properties.setProtocols(new String[]{"http", "https"});
        properties.setTitle("test title");
        properties.setDescription("test description");
        properties.setVersion("6.6.6");
        properties.setTermsOfServiceUrl("http://test.host.org/terms");
        properties.setContactName("test contact");
        properties.setContactEmail("test@host.org");
        properties.setContactUrl("http://test.host.org/contact");
        properties.setLicense("free as in beer");
        properties.setLicenseUrl("http://test.host.org/license");
        properties.setUseDefaultResponseMessages(false);

        config = new SwaggerAutoConfiguration(applicationProperties) {
            @Override
            protected Docket createDocket() {
                Docket docket = spy(super.createDocket());
                when(docket.select()).thenReturn(builder = spy(new ApiSelectorBuilder(docket)));
                return docket;
            }
        };
    }

    @Test
    public void testSwaggerSpringfoxApiDocket() {
        List<SwaggerCustomizer> customizers = Lists.newArrayList(new ApplicationSwaggerCustomizer(properties));
        Docket docket = config.swaggerSpringfoxApiDocket(customizers, new NullProvider<>());

        verify(docket, never()).groupName(anyString());
        verify(docket).host(properties.getHost());
        verify(docket).protocols(new HashSet<>(Arrays.asList(properties.getProtocols())));

        verify(docket).apiInfo(infoCaptor.capture());
        ApiInfo info = infoCaptor.getValue();
        assertThat(info.getTitle()).isEqualTo(properties.getTitle());
        assertThat(info.getDescription()).isEqualTo(properties.getDescription());
        assertThat(info.getVersion()).isEqualTo(properties.getVersion());
        assertThat(info.getTermsOfServiceUrl()).isEqualTo(properties.getTermsOfServiceUrl());
        assertThat(info.getContact().getName()).isEqualTo(properties.getContactName());
        assertThat(info.getContact().getEmail()).isEqualTo(properties.getContactEmail());
        assertThat(info.getContact().getUrl()).isEqualTo(properties.getContactUrl());
        assertThat(info.getLicense()).isEqualTo(properties.getLicense());
        assertThat(info.getLicenseUrl()).isEqualTo(properties.getLicenseUrl());
        assertThat(info.getVendorExtensions()).isEmpty();

        verify(docket).useDefaultResponseMessages(properties.isUseDefaultResponseMessages());
        verify(docket).forCodeGeneration(true);
        verify(docket).directModelSubstitute(ByteBuffer.class, String.class);
        verify(docket).genericModelSubstitutes(ResponseEntity.class);

        verify(docket).select();
        verify(builder).paths(pathsCaptor.capture());
        Predicate<String> paths = pathsCaptor.getValue();
        assertThat(paths.apply("/api/foo")).isEqualTo(true);
        assertThat(paths.apply("/foo/api")).isEqualTo(false);

        verify(builder).build();
    }

    @Test
    public void testSwaggerSpringfoxManagementDocket() {
        Docket docket = config.swaggerSpringfoxManagementDocket(properties.getTitle(), "/foo/");

        verify(docket).groupName(SwaggerAutoConfiguration.MANAGEMENT_GROUP_NAME);
        verify(docket).host(properties.getHost());
        verify(docket).protocols(new HashSet<>(Arrays.asList(properties.getProtocols())));

        verify(docket).apiInfo(infoCaptor.capture());
        ApiInfo info = infoCaptor.getValue();
        assertThat(info.getTitle()).isEqualTo(StringUtils.capitalize(properties.getTitle()) + " " +
                SwaggerAutoConfiguration.MANAGEMENT_TITLE_SUFFIX);
        assertThat(info.getDescription()).isEqualTo(SwaggerAutoConfiguration.MANAGEMENT_DESCRIPTION);
        assertThat(info.getVersion()).isEqualTo(properties.getVersion());
        assertThat(info.getTermsOfServiceUrl()).isEqualTo("");
        assertThat(info.getContact().getName()).isEqualTo(ApiInfo.DEFAULT_CONTACT.getName());
        assertThat(info.getContact().getEmail()).isEqualTo(ApiInfo.DEFAULT_CONTACT.getEmail());
        assertThat(info.getContact().getUrl()).isEqualTo(ApiInfo.DEFAULT_CONTACT.getUrl());
        assertThat(info.getLicense()).isEqualTo("");
        assertThat(info.getLicenseUrl()).isEqualTo("");
        assertThat(info.getVendorExtensions()).isEmpty();

        verify(docket).useDefaultResponseMessages(properties.isUseDefaultResponseMessages());
        verify(docket).forCodeGeneration(true);
        verify(docket).directModelSubstitute(ByteBuffer.class, String.class);
        verify(docket).genericModelSubstitutes(ResponseEntity.class);

        verify(docket).select();
        verify(builder).paths(pathsCaptor.capture());
        Predicate<String> paths = pathsCaptor.getValue();
        assertThat(paths.apply("/api/foo")).isEqualTo(false);
        assertThat(paths.apply("/foo/api")).isEqualTo(true);

        verify(builder).build();
    }

    static class NullProvider<T> implements ObjectProvider<T> {

        @Nullable
        @Override
        public T getObject(Object... args) throws BeansException {
            return null;
        }

        @Nullable
        @Override
        public T getIfAvailable() throws BeansException {
            return null;
        }

        @Nullable
        @Override
        public T getIfUnique() throws BeansException {
            return null;
        }

        @Nullable
        @Override
        public T getObject() throws BeansException {
            return null;
        }
    }
}