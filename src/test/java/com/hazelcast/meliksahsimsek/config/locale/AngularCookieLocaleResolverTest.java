package com.hazelcast.meliksahsimsek.config.locale;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.web.servlet.i18n.CookieLocaleResolver.DEFAULT_COOKIE_NAME;
import static org.springframework.web.servlet.i18n.CookieLocaleResolver.LOCALE_REQUEST_ATTRIBUTE_NAME;
import static org.springframework.web.servlet.i18n.CookieLocaleResolver.TIME_ZONE_REQUEST_ATTRIBUTE_NAME;

import java.time.ZoneId;
import java.util.Locale;
import java.util.TimeZone;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.MockitoAnnotations;
import org.springframework.context.i18n.LocaleContext;
import org.springframework.context.i18n.TimeZoneAwareLocaleContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

public class AngularCookieLocaleResolverTest {

    private static final Locale LOCALE_DEFAULT = Locale.UK;
    private static final Locale LOCALE_CUSTOM = Locale.FRANCE;
    private static final TimeZone TIMEZONE_CUSTOM = TimeZone.getTimeZone(ZoneId.of("GMT"));
    private static final TimeZone TIMEZONE_DEFAULT = TimeZone.getTimeZone(ZoneId.of("GMT+01:00"));

    private HttpServletRequest request;
    private HttpServletResponse response;
    private AngularCookieLocaleResolver resolver;

    @Captor
    private ArgumentCaptor<Cookie> captor;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);

        request = spy(new MockHttpServletRequest());

        response = spy(MockHttpServletResponse.class);
        resolver = new AngularCookieLocaleResolver();
        resolver.setDefaultLocale(LOCALE_DEFAULT);
        resolver.setDefaultTimeZone(TIMEZONE_DEFAULT);
    }

    @Test
    public void testDefaults() {
        when(request.getCookies()).thenReturn(new Cookie[]{});

        LocaleContext context = resolver.resolveLocaleContext(request);

        assertThat(context).isNotNull();
        assertThat(context).isInstanceOf(TimeZoneAwareLocaleContext.class);
        assertThat(((TimeZoneAwareLocaleContext) context).getLocale()).isEqualTo(LOCALE_DEFAULT);
        assertThat(((TimeZoneAwareLocaleContext) context).getTimeZone()).isEqualTo(TIMEZONE_DEFAULT);
    }

    @Test
    public void testPresets() {
        when(request.getAttribute(LOCALE_REQUEST_ATTRIBUTE_NAME)).thenReturn(LOCALE_DEFAULT);
        when(request.getAttribute(TIME_ZONE_REQUEST_ATTRIBUTE_NAME)).thenReturn(TIMEZONE_DEFAULT);

        LocaleContext context = resolver.resolveLocaleContext(request);

        assertThat(context).isNotNull();
        assertThat(context).isInstanceOf(TimeZoneAwareLocaleContext.class);
        Locale locale = ((TimeZoneAwareLocaleContext) context).getLocale();
        TimeZone zone = ((TimeZoneAwareLocaleContext) context).getTimeZone();

        assertThat(locale).isNotNull();
        assertThat(locale).isEqualTo(LOCALE_DEFAULT);
        assertThat(zone).isEqualTo(TIMEZONE_DEFAULT);
    }

    @Test
    public void testLocale() {
        String value = LOCALE_CUSTOM.toString();
        Cookie cookie = new Cookie(DEFAULT_COOKIE_NAME, value);
        when(request.getCookies()).thenReturn(new Cookie[]{cookie});

        Locale locale = resolver.resolveLocale(request);

        assertThat(locale).isNotNull();
        assertThat(locale).isEqualTo(LOCALE_CUSTOM);
    }

    @Test
    public void testCookieLocaleWithQuotes() {
        String value = resolver.quote(LOCALE_CUSTOM.toString());
        Cookie cookie = new Cookie(DEFAULT_COOKIE_NAME, value);
        when(request.getCookies()).thenReturn(new Cookie[]{cookie});

        Locale locale = resolver.resolveLocale(request);

        assertThat(locale).isNotNull();
        assertThat(locale).isEqualTo(LOCALE_CUSTOM);
    }

    @Test
    public void testTimeZone() {
        String value = "- " + TIMEZONE_CUSTOM.getID();
        Cookie cookie = new Cookie(DEFAULT_COOKIE_NAME, value);
        when(request.getCookies()).thenReturn(new Cookie[]{cookie});

        LocaleContext context = resolver.resolveLocaleContext(request);

        assertThat(context).isNotNull();
        assertThat(context).isInstanceOf(TimeZoneAwareLocaleContext.class);
        Locale locale = ((TimeZoneAwareLocaleContext) context).getLocale();
        TimeZone zone = ((TimeZoneAwareLocaleContext) context).getTimeZone();
        assertThat(locale).isEqualTo(LOCALE_DEFAULT);
        assertThat(zone).isEqualTo(TIMEZONE_CUSTOM);
    }

    @Test
    public void testTimeZoneWithQuotes() {
        String value = resolver.quote("- " + TIMEZONE_CUSTOM.getID());
        Cookie cookie = new Cookie(DEFAULT_COOKIE_NAME, value);
        when(request.getCookies()).thenReturn(new Cookie[]{cookie});

        LocaleContext context = resolver.resolveLocaleContext(request);

        assertThat(context).isNotNull();
        assertThat(context).isInstanceOf(TimeZoneAwareLocaleContext.class);
        Locale locale = ((TimeZoneAwareLocaleContext) context).getLocale();
        TimeZone zone = ((TimeZoneAwareLocaleContext) context).getTimeZone();
        assertThat(locale).isEqualTo(LOCALE_DEFAULT);
        assertThat(zone).isEqualTo(TIMEZONE_CUSTOM);
    }

    @Test
    public void testLocaleAndTimeZone() {
        String value = LOCALE_CUSTOM + " " + TIMEZONE_CUSTOM.getID();
        Cookie cookie = new Cookie(DEFAULT_COOKIE_NAME, value);
        when(request.getCookies()).thenReturn(new Cookie[]{cookie});

        LocaleContext context = resolver.resolveLocaleContext(request);

        assertThat(context).isNotNull();
        assertThat(context).isInstanceOf(TimeZoneAwareLocaleContext.class);
        Locale locale = ((TimeZoneAwareLocaleContext) context).getLocale();
        TimeZone zone = ((TimeZoneAwareLocaleContext) context).getTimeZone();
        assertThat(locale).isEqualTo(LOCALE_CUSTOM);
        assertThat(zone).isEqualTo(TIMEZONE_CUSTOM);
    }

    @Test
    public void testLocaleAndTimeZoneWithQuotes() {
        String value = resolver.quote(LOCALE_CUSTOM.toString() + " " + TIMEZONE_CUSTOM.getID());
        Cookie cookie = new Cookie(DEFAULT_COOKIE_NAME, value);
        when(request.getCookies()).thenReturn(new Cookie[]{cookie});

        LocaleContext context = resolver.resolveLocaleContext(request);

        assertThat(context).isNotNull();
        assertThat(context).isInstanceOf(TimeZoneAwareLocaleContext.class);
        Locale locale = ((TimeZoneAwareLocaleContext) context).getLocale();
        TimeZone zone = ((TimeZoneAwareLocaleContext) context).getTimeZone();
        assertThat(locale).isEqualTo(LOCALE_CUSTOM);
        assertThat(zone).isEqualTo(TIMEZONE_CUSTOM);
    }

    @Test
    public void testCookieWithQuotes() {
        String value = LOCALE_CUSTOM.toString();
        resolver.addCookie(response, value);

        verify(response).addCookie(captor.capture());

        Cookie cookie = captor.getValue();
        assertThat(cookie.getName()).isEqualTo(DEFAULT_COOKIE_NAME);
        assertThat(cookie.getValue()).isEqualTo(resolver.quote(value));
    }

    @Test
    public void testTraceLogLocale() {
        String value = LOCALE_CUSTOM.toString();
        Cookie cookie = new Cookie(DEFAULT_COOKIE_NAME, value);
        when(request.getCookies()).thenReturn(new Cookie[]{cookie});

        Locale locale = resolver.resolveLocale(request);
    }

    @Test
    public void testTraceLogLocaleAndTimeZone() {
        String value = LOCALE_CUSTOM + " " + TIMEZONE_CUSTOM.getID();
        Cookie cookie = new Cookie(DEFAULT_COOKIE_NAME, value);
        when(request.getCookies()).thenReturn(new Cookie[]{cookie});

        LocaleContext context = resolver.resolveLocaleContext(request);

        assertThat(context).isInstanceOf(TimeZoneAwareLocaleContext.class);
    }
}
