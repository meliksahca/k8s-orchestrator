package com.hazelcast.meliksahsimsek.config.liquibase;

import static com.hazelcast.meliksahsimsek.config.ApplicationConstants.SPRING_PROFILE_DEVELOPMENT;
import static com.hazelcast.meliksahsimsek.config.ApplicationConstants.SPRING_PROFILE_HEROKU;
import static com.hazelcast.meliksahsimsek.config.ApplicationConstants.SPRING_PROFILE_NO_LIQUIBASE;
import static com.hazelcast.meliksahsimsek.config.ApplicationConstants.SPRING_PROFILE_PRODUCTION;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mock.env.MockEnvironment;

public class AsyncSpringLiquibaseTest {

    private LiquibaseException exception = new LiquibaseException("Eek");

    private SimpleAsyncTaskExecutor executor;
    private ConfigurableEnvironment environment;
    private TestAsyncSpringLiquibase config;

    @BeforeEach
    public void setup() {
        executor = new SimpleAsyncTaskExecutor();
        environment = new MockEnvironment();
        config = spy(new TestAsyncSpringLiquibase(executor, environment));
    }

    @Test
    public void testProfileNoLiquibase() {
        environment.setActiveProfiles(SPRING_PROFILE_NO_LIQUIBASE);

        Throwable caught;
        synchronized (executor) {
            caught = catchThrowable(() -> {
                config.afterPropertiesSet();
                executor.wait(100);
            });
            assertThat(caught).isNull();
        }

        caught = catchThrowable(() -> verify(config, never()).initDb());
        assertThat(caught).isNull();
    }

    @Test
    public void testProfileProduction() {
        environment.setActiveProfiles(SPRING_PROFILE_PRODUCTION);

        Throwable caught;
        synchronized (executor) {
            caught = catchThrowable(() -> {
                config.afterPropertiesSet();
                executor.wait(100);
            });
            assertThat(caught).isNull();
        }

        caught = catchThrowable(() -> verify(config).initDb());
        assertThat(caught).isNull();
    }

    @Test
    public void testProfileDevelopment() {
        environment.setActiveProfiles(SPRING_PROFILE_DEVELOPMENT);

        Throwable caught;
        synchronized (executor) {
            caught = catchThrowable(() -> {
                config.afterPropertiesSet();
                executor.wait(100);
            });
            assertThat(caught).isNull();
        }

        caught = catchThrowable(() -> verify(config).initDb());
        assertThat(caught).isNull();
    }

    @Test
    public void testProfileHeroku() {
        environment.setActiveProfiles(SPRING_PROFILE_HEROKU);

        Throwable caught;
        synchronized (executor) {
            caught = catchThrowable(() -> {
                config.afterPropertiesSet();
                executor.wait(100);
            });
            assertThat(caught).isNull();
        }

        caught = catchThrowable(() -> verify(config).initDb());
        assertThat(caught).isNull();
    }

    @Test
    public void testSlow() {
        environment.setActiveProfiles(SPRING_PROFILE_DEVELOPMENT, SPRING_PROFILE_HEROKU);
        doReturn(AsyncSpringLiquibase.SLOWNESS_THRESHOLD * 1000L + 100L).when(config).getSleep();
        Throwable caught;

        synchronized (executor) {
            caught = catchThrowable(() -> {
                config.afterPropertiesSet();
                executor.wait(config.getSleep() + 100L);
            });
            assertThat(caught).isNull();
        }

        caught = catchThrowable(() -> verify(config).initDb());
        assertThat(caught).isNull();
    }

    @Test
    public void testException() {
        environment.setActiveProfiles(SPRING_PROFILE_DEVELOPMENT, SPRING_PROFILE_HEROKU);

        Throwable caught = catchThrowable(() -> doThrow(exception).when(config).initDb());
        assertThat(caught).isNull();

        synchronized (executor) {
            caught = catchThrowable(() -> {
                config.afterPropertiesSet();
                executor.wait(100);
            });
            assertThat(caught).isNull();
        }

        caught = catchThrowable(() -> verify(config).initDb());
        assertThat(caught).isNull();
    }

    private class TestAsyncSpringLiquibase
            extends AsyncSpringLiquibase {

        public TestAsyncSpringLiquibase(TaskExecutor executor, Environment environment) {
            super(executor, environment);
        }

        @Override
        protected void initDb() throws LiquibaseException {
            synchronized (executor) {
                super.initDb();
                executor.notifyAll();
            }
        }

        @Override
        public DataSource getDataSource() {
            DataSource source = mock(DataSource.class);
            try {
                doReturn(mock(Connection.class)).when(source).getConnection();
            } catch (SQLException x) {
                // This should never happen
                throw new Error(x);
            }
            return source;
        }

        @Override
        protected Liquibase createLiquibase(Connection c) {
            return null;
        }

        @Override
        protected void performUpdate(Liquibase liquibase) {
            long sleep = getSleep();
            if (sleep > 0) {
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException x) {
                    // This should never happen
                    throw new Error(x);
                }
            }
        }

        long getSleep() {
            return 0L;
        }
    }
}
