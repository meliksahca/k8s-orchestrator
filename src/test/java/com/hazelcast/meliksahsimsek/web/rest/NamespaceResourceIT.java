package com.hazelcast.meliksahsimsek.web.rest;

import com.hazelcast.meliksahsimsek.K8SOrchestratorApp;
import com.hazelcast.meliksahsimsek.domain.Namespace;
import com.hazelcast.meliksahsimsek.repository.NamespaceRepository;
import com.hazelcast.meliksahsimsek.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.hazelcast.meliksahsimsek.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link NamespaceResource} REST controller.
 */
@SpringBootTest(classes = K8SOrchestratorApp.class)
public class NamespaceResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private NamespaceRepository namespaceRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restNamespaceMockMvc;

    private Namespace namespace;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NamespaceResource namespaceResource = new NamespaceResource(namespaceRepository);
        this.restNamespaceMockMvc = MockMvcBuilders.standaloneSetup(namespaceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Namespace createEntity(EntityManager em) {
        Namespace namespace = new Namespace()
            .name(DEFAULT_NAME);
        return namespace;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Namespace createUpdatedEntity(EntityManager em) {
        Namespace namespace = new Namespace()
            .name(UPDATED_NAME);
        return namespace;
    }

    @BeforeEach
    public void initTest() {
        namespace = createEntity(em);
    }

    @Test
    @Transactional
    public void createNamespace() throws Exception {
        int databaseSizeBeforeCreate = namespaceRepository.findAll().size();

        // Create the Namespace
        restNamespaceMockMvc.perform(post("/api/namespaces")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(namespace)))
            .andExpect(status().isCreated());

        // Validate the Namespace in the database
        List<Namespace> namespaceList = namespaceRepository.findAll();
        assertThat(namespaceList).hasSize(databaseSizeBeforeCreate + 1);
        Namespace testNamespace = namespaceList.get(namespaceList.size() - 1);
        assertThat(testNamespace.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createNamespaceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = namespaceRepository.findAll().size();

        // Create the Namespace with an existing ID
        namespace.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNamespaceMockMvc.perform(post("/api/namespaces")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(namespace)))
            .andExpect(status().isBadRequest());

        // Validate the Namespace in the database
        List<Namespace> namespaceList = namespaceRepository.findAll();
        assertThat(namespaceList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllNamespaces() throws Exception {
        // Initialize the database
        namespaceRepository.saveAndFlush(namespace);

        // Get all the namespaceList
        restNamespaceMockMvc.perform(get("/api/namespaces?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(namespace.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
    
    @Test
    @Transactional
    public void getNamespace() throws Exception {
        // Initialize the database
        namespaceRepository.saveAndFlush(namespace);

        // Get the namespace
        restNamespaceMockMvc.perform(get("/api/namespaces/{id}", namespace.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(namespace.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingNamespace() throws Exception {
        // Get the namespace
        restNamespaceMockMvc.perform(get("/api/namespaces/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNamespace() throws Exception {
        // Initialize the database
        namespaceRepository.saveAndFlush(namespace);

        int databaseSizeBeforeUpdate = namespaceRepository.findAll().size();

        // Update the namespace
        Namespace updatedNamespace = namespaceRepository.findById(namespace.getId()).get();
        // Disconnect from session so that the updates on updatedNamespace are not directly saved in db
        em.detach(updatedNamespace);
        updatedNamespace
            .name(UPDATED_NAME);

        restNamespaceMockMvc.perform(put("/api/namespaces")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNamespace)))
            .andExpect(status().isOk());

        // Validate the Namespace in the database
        List<Namespace> namespaceList = namespaceRepository.findAll();
        assertThat(namespaceList).hasSize(databaseSizeBeforeUpdate);
        Namespace testNamespace = namespaceList.get(namespaceList.size() - 1);
        assertThat(testNamespace.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingNamespace() throws Exception {
        int databaseSizeBeforeUpdate = namespaceRepository.findAll().size();

        // Create the Namespace

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNamespaceMockMvc.perform(put("/api/namespaces")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(namespace)))
            .andExpect(status().isBadRequest());

        // Validate the Namespace in the database
        List<Namespace> namespaceList = namespaceRepository.findAll();
        assertThat(namespaceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNamespace() throws Exception {
        // Initialize the database
        namespaceRepository.saveAndFlush(namespace);

        int databaseSizeBeforeDelete = namespaceRepository.findAll().size();

        // Delete the namespace
        restNamespaceMockMvc.perform(delete("/api/namespaces/{id}", namespace.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Namespace> namespaceList = namespaceRepository.findAll();
        assertThat(namespaceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Namespace.class);
        Namespace namespace1 = new Namespace();
        namespace1.setId(1L);
        Namespace namespace2 = new Namespace();
        namespace2.setId(namespace1.getId());
        assertThat(namespace1).isEqualTo(namespace2);
        namespace2.setId(2L);
        assertThat(namespace1).isNotEqualTo(namespace2);
        namespace1.setId(null);
        assertThat(namespace1).isNotEqualTo(namespace2);
    }
}
