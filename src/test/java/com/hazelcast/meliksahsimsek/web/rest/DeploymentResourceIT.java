package com.hazelcast.meliksahsimsek.web.rest;

import com.hazelcast.meliksahsimsek.K8SOrchestratorApp;
import com.hazelcast.meliksahsimsek.domain.Deployment;
import com.hazelcast.meliksahsimsek.repository.DeploymentRepository;
import com.hazelcast.meliksahsimsek.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.hazelcast.meliksahsimsek.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DeploymentResource} REST controller.
 */
@SpringBootTest(classes = K8SOrchestratorApp.class)
public class DeploymentResourceIT {

    private static final String DEFAULT_API_VERSION = "AAAAAAAAAA";
    private static final String UPDATED_API_VERSION = "BBBBBBBBBB";

    private static final String DEFAULT_KIND = "AAAAAAAAAA";
    private static final String UPDATED_KIND = "BBBBBBBBBB";

    private static final String DEFAULT_METADATA_NAME = "AAAAAAAAAA";
    private static final String UPDATED_METADATA_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LABEL_KEY = "AAAAAAAAAA";
    private static final String UPDATED_LABEL_KEY = "BBBBBBBBBB";

    private static final String DEFAULT_LABEL_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_LABEL_VALUE = "BBBBBBBBBB";

    private static final Integer DEFAULT_REPLICA = 1;
    private static final Integer UPDATED_REPLICA = 2;
    private static final Integer SMALLER_REPLICA = 1 - 1;

    @Autowired
    private DeploymentRepository deploymentRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDeploymentMockMvc;

    private Deployment deployment;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DeploymentResource deploymentResource = new DeploymentResource(deploymentRepository);
        this.restDeploymentMockMvc = MockMvcBuilders.standaloneSetup(deploymentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Deployment createEntity(EntityManager em) {
        Deployment deployment = new Deployment()
            .apiVersion(DEFAULT_API_VERSION)
            .kind(DEFAULT_KIND)
            .metadataName(DEFAULT_METADATA_NAME)
            .labelKey(DEFAULT_LABEL_KEY)
            .labelValue(DEFAULT_LABEL_VALUE)
            .replica(DEFAULT_REPLICA);
        return deployment;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Deployment createUpdatedEntity(EntityManager em) {
        Deployment deployment = new Deployment()
            .apiVersion(UPDATED_API_VERSION)
            .kind(UPDATED_KIND)
            .metadataName(UPDATED_METADATA_NAME)
            .labelKey(UPDATED_LABEL_KEY)
            .labelValue(UPDATED_LABEL_VALUE)
            .replica(UPDATED_REPLICA);
        return deployment;
    }

    @BeforeEach
    public void initTest() {
        deployment = createEntity(em);
    }

    @Test
    @Transactional
    public void createDeployment() throws Exception {
        int databaseSizeBeforeCreate = deploymentRepository.findAll().size();

        // Create the Deployment
        restDeploymentMockMvc.perform(post("/api/deployments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deployment)))
            .andExpect(status().isCreated());

        // Validate the Deployment in the database
        List<Deployment> deploymentList = deploymentRepository.findAll();
        assertThat(deploymentList).hasSize(databaseSizeBeforeCreate + 1);
        Deployment testDeployment = deploymentList.get(deploymentList.size() - 1);
        assertThat(testDeployment.getApiVersion()).isEqualTo(DEFAULT_API_VERSION);
        assertThat(testDeployment.getKind()).isEqualTo(DEFAULT_KIND);
        assertThat(testDeployment.getMetadataName()).isEqualTo(DEFAULT_METADATA_NAME);
        assertThat(testDeployment.getLabelKey()).isEqualTo(DEFAULT_LABEL_KEY);
        assertThat(testDeployment.getLabelValue()).isEqualTo(DEFAULT_LABEL_VALUE);
        assertThat(testDeployment.getReplica()).isEqualTo(DEFAULT_REPLICA);
    }

    @Test
    @Transactional
    public void createDeploymentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = deploymentRepository.findAll().size();

        // Create the Deployment with an existing ID
        deployment.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDeploymentMockMvc.perform(post("/api/deployments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deployment)))
            .andExpect(status().isBadRequest());

        // Validate the Deployment in the database
        List<Deployment> deploymentList = deploymentRepository.findAll();
        assertThat(deploymentList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDeployments() throws Exception {
        // Initialize the database
        deploymentRepository.saveAndFlush(deployment);

        // Get all the deploymentList
        restDeploymentMockMvc.perform(get("/api/deployments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(deployment.getId().intValue())))
            .andExpect(jsonPath("$.[*].apiVersion").value(hasItem(DEFAULT_API_VERSION.toString())))
            .andExpect(jsonPath("$.[*].kind").value(hasItem(DEFAULT_KIND.toString())))
            .andExpect(jsonPath("$.[*].metadataName").value(hasItem(DEFAULT_METADATA_NAME.toString())))
            .andExpect(jsonPath("$.[*].labelKey").value(hasItem(DEFAULT_LABEL_KEY.toString())))
            .andExpect(jsonPath("$.[*].labelValue").value(hasItem(DEFAULT_LABEL_VALUE.toString())))
            .andExpect(jsonPath("$.[*].replica").value(hasItem(DEFAULT_REPLICA)));
    }
    
    @Test
    @Transactional
    public void getDeployment() throws Exception {
        // Initialize the database
        deploymentRepository.saveAndFlush(deployment);

        // Get the deployment
        restDeploymentMockMvc.perform(get("/api/deployments/{id}", deployment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(deployment.getId().intValue()))
            .andExpect(jsonPath("$.apiVersion").value(DEFAULT_API_VERSION.toString()))
            .andExpect(jsonPath("$.kind").value(DEFAULT_KIND.toString()))
            .andExpect(jsonPath("$.metadataName").value(DEFAULT_METADATA_NAME.toString()))
            .andExpect(jsonPath("$.labelKey").value(DEFAULT_LABEL_KEY.toString()))
            .andExpect(jsonPath("$.labelValue").value(DEFAULT_LABEL_VALUE.toString()))
            .andExpect(jsonPath("$.replica").value(DEFAULT_REPLICA));
    }

    @Test
    @Transactional
    public void getNonExistingDeployment() throws Exception {
        // Get the deployment
        restDeploymentMockMvc.perform(get("/api/deployments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDeployment() throws Exception {
        // Initialize the database
        deploymentRepository.saveAndFlush(deployment);

        int databaseSizeBeforeUpdate = deploymentRepository.findAll().size();

        // Update the deployment
        Deployment updatedDeployment = deploymentRepository.findById(deployment.getId()).get();
        // Disconnect from session so that the updates on updatedDeployment are not directly saved in db
        em.detach(updatedDeployment);
        updatedDeployment
            .apiVersion(UPDATED_API_VERSION)
            .kind(UPDATED_KIND)
            .metadataName(UPDATED_METADATA_NAME)
            .labelKey(UPDATED_LABEL_KEY)
            .labelValue(UPDATED_LABEL_VALUE)
            .replica(UPDATED_REPLICA);

        restDeploymentMockMvc.perform(put("/api/deployments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDeployment)))
            .andExpect(status().isOk());

        // Validate the Deployment in the database
        List<Deployment> deploymentList = deploymentRepository.findAll();
        assertThat(deploymentList).hasSize(databaseSizeBeforeUpdate);
        Deployment testDeployment = deploymentList.get(deploymentList.size() - 1);
        assertThat(testDeployment.getApiVersion()).isEqualTo(UPDATED_API_VERSION);
        assertThat(testDeployment.getKind()).isEqualTo(UPDATED_KIND);
        assertThat(testDeployment.getMetadataName()).isEqualTo(UPDATED_METADATA_NAME);
        assertThat(testDeployment.getLabelKey()).isEqualTo(UPDATED_LABEL_KEY);
        assertThat(testDeployment.getLabelValue()).isEqualTo(UPDATED_LABEL_VALUE);
        assertThat(testDeployment.getReplica()).isEqualTo(UPDATED_REPLICA);
    }

    @Test
    @Transactional
    public void updateNonExistingDeployment() throws Exception {
        int databaseSizeBeforeUpdate = deploymentRepository.findAll().size();

        // Create the Deployment

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDeploymentMockMvc.perform(put("/api/deployments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deployment)))
            .andExpect(status().isBadRequest());

        // Validate the Deployment in the database
        List<Deployment> deploymentList = deploymentRepository.findAll();
        assertThat(deploymentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDeployment() throws Exception {
        // Initialize the database
        deploymentRepository.saveAndFlush(deployment);

        int databaseSizeBeforeDelete = deploymentRepository.findAll().size();

        // Delete the deployment
        restDeploymentMockMvc.perform(delete("/api/deployments/{id}", deployment.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Deployment> deploymentList = deploymentRepository.findAll();
        assertThat(deploymentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Deployment.class);
        Deployment deployment1 = new Deployment();
        deployment1.setId(1L);
        Deployment deployment2 = new Deployment();
        deployment2.setId(deployment1.getId());
        assertThat(deployment1).isEqualTo(deployment2);
        deployment2.setId(2L);
        assertThat(deployment1).isNotEqualTo(deployment2);
        deployment1.setId(null);
        assertThat(deployment1).isNotEqualTo(deployment2);
    }
}
