#!/bin/sh

echo "The application will start in ${HCM_SLEEP}s..." && sleep ${HCM_SLEEP}
exec java ${JAVA_OPTS} -noverify -XX:+AlwaysPreTouch -Djava.security.egd=file:/dev/./urandom -cp /app/resources/:/app/classes/:/app/libs/* "com.hazelcast.meliksahsimsek.K8SOrchestratorApp"  "$@"
