package com.hazelcast.meliksahsimsek.config.metric;

import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.boot.actuate.autoconfigure.metrics.MetricsEndpointAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>HcmMetricsEndpointConfiguration class.</p>
 */
@Configuration
@ConditionalOnClass(Timed.class)
@AutoConfigureAfter(MetricsEndpointAutoConfiguration.class)
public class HcmMetricsEndpointConfiguration {

    /**
     * <p>HcmMetricsEndpoint.</p>
     *
     * @param meterRegistry a {@link io.micrometer.core.instrument.MeterRegistry} object.
     * @return a {@link com.hazelcast.meliksahsimsek.config.metric.HcmMetricsEndpoint} object.
     */
    @Bean
    public HcmMetricsEndpoint HcmMetricsEndpoint(MeterRegistry meterRegistry) {
        return new HcmMetricsEndpoint(meterRegistry);
    }
}
