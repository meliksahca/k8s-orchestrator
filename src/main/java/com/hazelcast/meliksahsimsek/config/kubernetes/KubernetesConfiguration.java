package com.hazelcast.meliksahsimsek.config;


import com.hazelcast.meliksahsimsek.config.kubernetes.KubernetesClientProperties;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.ConfigBuilder;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(value = "spring.cloud.kubernetes.enabled", matchIfMissing = true)
@EnableConfigurationProperties(KubernetesClientProperties.class)
public class KubernetesConfiguration {

    private static final Log LOG = LogFactory.getLog(KubernetesClientProperties.class);

    @Bean
    @ConditionalOnMissingBean(Config.class)
    public Config kubernetesClientConfig(KubernetesClientProperties kubernetesClientProperties) {

        // loading the out-of-cluster config, a kubeconfig from file-system
        /*ApiClient defaultClient = io.kubernetes.client.Configuration.getDefaultApiClient();
        defaultClient.setBasePath(kubernetesClientProperties.getMasterUrl());
        defaultClient.setVerifyingSsl(false);
        ApiKeyAuth bearerToken = (ApiKeyAuth) defaultClient.getAuthentication("BearerToken");
        bearerToken.setApiKey(kubernetesClientProperties.getOAuthToken());
        bearerToken.setApiKeyPrefix("Bearer");

        ApiClient realClient = ClientBuilder.kubeconfig(KubeConfig.loadKubeConfig(new FileReader("/Users/meliksah/.kube/config"))).build();
        // set the global default api-client to the in-cluster one from above
        CoreV1Api coreV1Api = new CoreV1Api();
        AuthenticationV1Api apiInstance = new AuthenticationV1Api();
        V1TokenReview body = new V1TokenReview(); // V1TokenReview |
        String dryRun = "dryRun_example"; // String | When present, indicates that modifications should not be persisted. An invalid or unrecognized dryRun directive will result in an error response and no further processing of the request. Valid values are: - All: all dry run stages will be processed
        String fieldManager = "fieldManager_example"; // String | fieldManager is a name associated with the actor or entity that is making these changes. The value must be less than or 128 characters long, and only contain printable characters, as defined by https://golang.org/pkg/unicode/#IsPrint.
        String pretty = "pretty_example"; //
        V1TokenReview result = apiInstance.createTokenReview(body, dryRun, fieldManager, pretty);

        // the CoreV1Api loads default api-client from global configuration.
        CoreV1Api api = new CoreV1Api();*/
        Config base = new Config();
        Config properties = new ConfigBuilder(base)
                //Only set values that have been explicitly specified
                .withMasterUrl(or(kubernetesClientProperties.getMasterUrl(), base.getMasterUrl()))
                .withOauthToken(or(kubernetesClientProperties.getOAuthToken(), base.getOauthToken()))
                .withApiVersion(or(kubernetesClientProperties.getApiVersion(), base.getApiVersion()))
                .withNamespace(or(kubernetesClientProperties.getNamespace(), base.getNamespace()))
                .withUsername(or(kubernetesClientProperties.getUsername(), base.getUsername()))
                .withPassword(or(kubernetesClientProperties.getPassword(), base.getPassword()))

                .withCaCertFile(or(kubernetesClientProperties.getCaCertFile(), base.getCaCertFile()))
                .withCaCertData(or(kubernetesClientProperties.getCaCertData(), base.getCaCertData()))

                .withClientKeyFile(or(kubernetesClientProperties.getClientKeyFile(), base.getClientKeyFile()))
                .withClientKeyData(or(kubernetesClientProperties.getClientKeyData(), base.getClientKeyData()))

                .withClientCertFile(or(kubernetesClientProperties.getClientCertFile(), base.getClientCertFile()))
                .withClientCertData(or(kubernetesClientProperties.getClientCertData(), base.getClientCertData()))

                //No magic is done for the properties below so we leave them as is.
                .withClientKeyAlgo(or(kubernetesClientProperties.getClientKeyAlgo(), base.getClientKeyAlgo()))
                .withClientKeyPassphrase(or(kubernetesClientProperties.getClientKeyPassphrase(), base.getClientKeyPassphrase()))
                .withConnectionTimeout(or(kubernetesClientProperties.getConnectionTimeout(), base.getConnectionTimeout()))
                .withRequestTimeout(or(kubernetesClientProperties.getRequestTimeout(), base.getRequestTimeout()))
                .withRollingTimeout(or(kubernetesClientProperties.getRollingTimeout(), base.getRollingTimeout()))
                .withTrustCerts(or(kubernetesClientProperties.isTrustCerts(), base.isTrustCerts()))
                .build();

        if (properties.getNamespace() == null || properties.getNamespace().isEmpty()) {
            LOG.warn("No namespace has been detected. Please specify KUBERNETES_NAMESPACE env var, or use a later kubernetes version (1.3 or later)");
        }
        return properties;
    }

    @Bean
    @ConditionalOnMissingBean
    public KubernetesClient kubernetesClient(Config config) {
        return new DefaultKubernetesClient(config);
    }

    private static <D> D or(D dis, D dat) {
        if (dis != null) {
            return dis;
        } else {
            return dat;
        }
    }
}
