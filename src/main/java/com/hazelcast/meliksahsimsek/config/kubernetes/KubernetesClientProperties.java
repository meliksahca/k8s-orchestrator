package com.hazelcast.meliksahsimsek.config.kubernetes;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("spring.cloud.kubernetes.client")
public class KubernetesClientProperties {

    private Boolean trustCerts;

    private String masterUrl;

    private String apiVersion;

    private String namespace;

    private String caCertFile;

    private String caCertData;

    private String clientCertFile;

    private String clientCertData;

    private String clientKeyFile;

    private String clientKeyData;

    private String clientKeyAlgo;

    private String clientKeyPassphrase;

    private String username;

    private String password;

    private String oAuthToken;

    private int watchReconnectInterval;

    private int watchReconnectLimit;

    private int connectionTimeout;

    private int requestTimeout;

    private long rollingTimeout;

    private int loggingInterval;

    private String httpProxy;

    private String httpsProxy;

    private String proxyUsername;

    private String proxyPassword;

    private String[] noProxy;

    public String getClientCertData() {
        return this.clientCertData;
    }

    public void setClientCertData(String clientCertData) {
        this.clientCertData = clientCertData;
    }

    public Boolean isTrustCerts() {
        return this.trustCerts;
    }

    public String getMasterUrl() {
        return this.masterUrl;
    }

    public void setMasterUrl(String masterUrl) {
        this.masterUrl = masterUrl;
    }

    public String getApiVersion() {
        return this.apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getNamespace() {
        return this.namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getCaCertFile() {
        return this.caCertFile;
    }

    public void setCaCertFile(String caCertFile) {
        this.caCertFile = caCertFile;
    }

    public String getCaCertData() {
        return this.caCertData;
    }

    public void setCaCertData(String caCertData) {
        this.caCertData = caCertData;
    }

    public String getClientCertFile() {
        return this.clientCertFile;
    }

    public void setClientCertFile(String clientCertFile) {
        this.clientCertFile = clientCertFile;
    }

    public String getClientKeyFile() {
        return this.clientKeyFile;
    }

    public void setClientKeyFile(String clientKeyFile) {
        this.clientKeyFile = clientKeyFile;
    }

    public String getClientKeyData() {
        return this.clientKeyData;
    }

    public void setClientKeyData(String clientKeyData) {
        this.clientKeyData = clientKeyData;
    }

    public String getClientKeyAlgo() {
        return this.clientKeyAlgo;
    }

    public void setClientKeyAlgo(String clientKeyAlgo) {
        this.clientKeyAlgo = clientKeyAlgo;
    }

    public String getClientKeyPassphrase() {
        return this.clientKeyPassphrase;
    }

    public void setClientKeyPassphrase(String clientKeyPassphrase) {
        this.clientKeyPassphrase = clientKeyPassphrase;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getWatchReconnectInterval() {
        return this.watchReconnectInterval;
    }

    public void setWatchReconnectInterval(int watchReconnectInterval) {
        this.watchReconnectInterval = watchReconnectInterval;
    }

    public int getWatchReconnectLimit() {
        return this.watchReconnectLimit;
    }

    public void setWatchReconnectLimit(int watchReconnectLimit) {
        this.watchReconnectLimit = watchReconnectLimit;
    }

    public int getConnectionTimeout() {
        return this.connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public int getRequestTimeout() {
        return this.requestTimeout;
    }

    public void setRequestTimeout(int requestTimeout) {
        this.requestTimeout = requestTimeout;
    }

    public long getRollingTimeout() {
        return this.rollingTimeout;
    }

    public void setRollingTimeout(long rollingTimeout) {
        this.rollingTimeout = rollingTimeout;
    }

    public int getLoggingInterval() {
        return this.loggingInterval;
    }

    public void setLoggingInterval(int loggingInterval) {
        this.loggingInterval = loggingInterval;
    }

    public Boolean getTrustCerts() {
        return this.trustCerts;
    }

    public void setTrustCerts(Boolean trustCerts) {
        this.trustCerts = trustCerts;
    }

    public String getHttpProxy() {
        return this.httpProxy;
    }

    public void setHttpProxy(String httpProxy) {
        this.httpProxy = httpProxy;
    }

    public String getHttpsProxy() {
        return this.httpsProxy;
    }

    public void setHttpsProxy(String httpsProxy) {
        this.httpsProxy = httpsProxy;
    }

    public String getProxyUsername() {
        return this.proxyUsername;
    }

    public void setProxyUsername(String proxyUsername) {
        this.proxyUsername = proxyUsername;
    }

    public String getProxyPassword() {
        return this.proxyPassword;
    }

    public void setProxyPassword(String proxyPassword) {
        this.proxyPassword = proxyPassword;
    }

    public String[] getNoProxy() {
        return this.noProxy;
    }

    public void setNoProxy(String[] noProxy) {
        this.noProxy = noProxy;
    }


    public String getOAuthToken() {
        return oAuthToken;
    }

    public void setOAuthToken(String oAuthToken) {
        this.oAuthToken = oAuthToken;
    }

}
