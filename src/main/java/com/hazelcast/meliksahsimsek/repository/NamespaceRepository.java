package com.hazelcast.meliksahsimsek.repository;

import com.hazelcast.meliksahsimsek.domain.Namespace;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Namespace entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NamespaceRepository extends JpaRepository<Namespace, Long> {

    Optional<Namespace> findByNameEquals(String name);

    void deleteByName(String name);

    default Namespace saveByName(Namespace namespace) {
        Optional<Namespace> namespacePersisted = findByNameEquals(namespace.getName());
        namespacePersisted.ifPresent(namespace1 -> namespace.setId(namespace1.getId()));
        return save(namespace);
    }

    default List<Namespace> saveAllByName(Iterable<Namespace> namespaces) {
        namespaces.forEach(namespace -> {
            findByNameEquals(namespace.getName()).ifPresent(namespace1 -> namespace.setId(namespace1.getId()));
        });
        return saveAll(namespaces);
    }
}
