package com.hazelcast.meliksahsimsek.repository;

import com.hazelcast.meliksahsimsek.domain.Container;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Container entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContainerRepository extends JpaRepository<Container, Long> {

    Optional<Container> findByNameAndAndImage(String name, String image);

    List<Container> findAllByDeployment_Namespace_NameAndDeployment_MetadataName(String namespace, String metadataName);

    default Container saveByNameAndImage(Container container) {
        findByNameAndAndImage(container.getName(), container.getImage())
                .ifPresent(containerPersisted -> container.setId(containerPersisted.getId()));
        return save(container);
    }

    default List<Container> saveAllByNameAndImage(Iterable<Container> containers) {
        containers.forEach(container -> {
            findByNameAndAndImage(container.getName(), container.getImage())
                    .ifPresent(containerPersisted -> container.setId(containerPersisted.getId()));
        });
        return saveAll(containers);
    }
}
