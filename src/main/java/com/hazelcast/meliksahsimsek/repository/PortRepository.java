package com.hazelcast.meliksahsimsek.repository;

import com.hazelcast.meliksahsimsek.domain.Port;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Port entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PortRepository extends JpaRepository<Port, Long> {

    Optional<Port> findByPortAndContainerNameAndContainerImage(String port, String containerName,
            String containerImage);

    default Port saveByPortAndContainerNameAndContainerImage(Port port) {
        findByPortAndContainerNameAndContainerImage(port.getPort(), port.getContainer().getName(),
                port.getContainer().getImage()).ifPresent(portPersisted -> port.setId(portPersisted.getId()));
        return save(port);
    }

    default List<Port> saveAllByPortAndContainerNameAndContainerImage(Iterable<Port> ports) {
        ports.forEach(port -> {
            findByPortAndContainerNameAndContainerImage(port.getPort(), port.getContainer().getName(),
                    port.getContainer().getImage()).ifPresent(portPersisted -> port.setId(portPersisted.getId()));
        });
        return saveAll(ports);
    }

}
