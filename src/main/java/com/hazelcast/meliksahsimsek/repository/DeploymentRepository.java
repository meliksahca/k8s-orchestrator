package com.hazelcast.meliksahsimsek.repository;

import com.hazelcast.meliksahsimsek.domain.Deployment;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Deployment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DeploymentRepository extends JpaRepository<Deployment, Long> {

    List<Deployment> findAllByNamespace_Name(String nameSpaceName);

    Optional<Deployment> findByMetadataNameEqualsAndNamespace_NameEquals(String metadataName, String namespace);

    void deleteByMetadataNameAndNamespace_Name(String metadataName, String namespace);

    default Deployment saveByMetadataNameAndNamspace_Name(Deployment deployment) {
        findByMetadataNameEqualsAndNamespace_NameEquals(deployment.getMetadataName(), deployment.getNamespace().getName())
                .ifPresent(deploymentPersisted -> deployment.setId(deploymentPersisted.getId()));
        return save(deployment);
    }

    default List<Deployment> saveAllMetadataNameAndNamspace_Name(Iterable<Deployment> deployments) {
        deployments.forEach(deployment -> {
            findByMetadataNameEqualsAndNamespace_NameEquals(deployment.getMetadataName(), deployment.getNamespace().getName())
                    .ifPresent(deploymentPersisted -> deployment.setId(deploymentPersisted.getId()));
        });
        return saveAll(deployments);
    }
}
