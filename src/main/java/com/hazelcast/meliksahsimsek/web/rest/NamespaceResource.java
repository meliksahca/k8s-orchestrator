package com.hazelcast.meliksahsimsek.web.rest;

import com.hazelcast.meliksahsimsek.domain.Namespace;
import com.hazelcast.meliksahsimsek.repository.NamespaceRepository;
import com.hazelcast.meliksahsimsek.service.KubernetesService;
import com.hazelcast.meliksahsimsek.web.rest.errors.BadRequestAlertException;
import com.hazelcast.meliksahsimsek.web.util.HeaderUtil;
import com.hazelcast.meliksahsimsek.web.util.ResponseUtil;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing {@link com.hazelcast.meliksahsimsek.domain.Namespace}.
 */
@RestController
@RequestMapping("/api")
public class NamespaceResource {

    private static final String ENTITY_NAME = "namespace";
    private final Logger log = LoggerFactory.getLogger(NamespaceResource.class);
    private final NamespaceRepository namespaceRepository;
    private final KubernetesService kubernetesService;
    @Value("${application.clientApp.name}")
    private String applicationName;

    public NamespaceResource(NamespaceRepository namespaceRepository, KubernetesService kubernetesService) {
        this.namespaceRepository = namespaceRepository;
        this.kubernetesService = kubernetesService;
    }

    /**
     * {@code POST  /namespaces} : Create a new namespace.
     *
     * @param namespace the namespace to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new namespace, or with
     * status {@code 400 (Bad Request)} if the namespace has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */

    @PostMapping("/namespaces")
    public ResponseEntity<Namespace> createNamespace(@RequestBody Namespace namespace) throws URISyntaxException {
        log.debug("REST request to save Namespace : {}", namespace);
        if (namespace.getId() != null) {
            throw new BadRequestAlertException("A new namespace cannot already have an ID", ENTITY_NAME, "idexists");
        }
        kubernetesService.createOrUpdateNamespace(namespace);
        Namespace result = namespaceRepository.save(namespace);
        return ResponseEntity.created(new URI("/api/namespaces/" + result.getId()))
                .headers(HeaderUtil
                        .createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * {@code PUT  /namespaces} : Updates an existing namespace.
     *
     * @param namespace the namespace to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated namespace, or with
     * status {@code 400 (Bad Request)} if the namespace is not valid, or with status {@code 500 (Internal Server
     * Error)} if the namespace couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/namespaces")
    public ResponseEntity<Namespace> updateNamespace(@RequestBody Namespace namespace) throws URISyntaxException {
        log.debug("REST request to update Namespace : {}", namespace);
        if (namespace.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        kubernetesService.createOrUpdateNamespace(namespace);
        Namespace result = namespaceRepository.save(namespace);
        return ResponseEntity.ok()
                .headers(HeaderUtil
                        .createEntityUpdateAlert(applicationName, false, ENTITY_NAME, namespace.getId().toString()))
                .body(result);
    }

    /**
     * {@code GET  /namespaces} : get all the namespaces.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of namespaces in body.
     */
    @GetMapping("/namespaces")
    public List<Namespace> getAllNamespaces() {
        log.debug("REST request to get all Namespaces");
        kubernetesService.getAndUpdateNamespaces();
        return namespaceRepository.findAll();
    }

    /**
     * {@code GET  /namespaces/:id} : get the "id" namespace.
     *
     * @param id the id of the namespace to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the namespace, or with status
     * {@code 404 (Not Found)}.
     */
    @GetMapping("/namespaces/{id}")
    public ResponseEntity<Namespace> getNamespace(@PathVariable Long id) {
        log.debug("REST request to get Namespace : {}", id);
        Namespace namespace = namespaceRepository.findById(id).orElseThrow(NoSuchElementException::new);
        kubernetesService.getAndUpdateNamespace(namespace.getName());
        return ResponseUtil.wrapOrNotFound(Optional.of(namespace));
    }

    /**
     * {@code DELETE  /namespaces/:id} : delete the "id" namespace.
     *
     * @param id the id of the namespace to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/namespaces/{id}")
    public ResponseEntity<Void> deleteNamespace(@PathVariable Long id) {
        log.debug("REST request to delete Namespace : {}", id);
        Namespace namespace = namespaceRepository.findById(id).orElseThrow(NoSuchElementException::new);
        kubernetesService.deleteNamespace(namespace.getName());
        namespaceRepository.deleteByName(namespace.getName());
        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
                .build();
    }
}
