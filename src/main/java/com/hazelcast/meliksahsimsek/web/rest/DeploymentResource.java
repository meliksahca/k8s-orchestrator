package com.hazelcast.meliksahsimsek.web.rest;

import com.hazelcast.meliksahsimsek.domain.Deployment;
import com.hazelcast.meliksahsimsek.repository.DeploymentRepository;
import com.hazelcast.meliksahsimsek.service.DeploymentService;
import com.hazelcast.meliksahsimsek.service.KubernetesService;
import com.hazelcast.meliksahsimsek.service.mapper.DeploymentMapper;
import com.hazelcast.meliksahsimsek.web.rest.errors.BadRequestAlertException;
import com.hazelcast.meliksahsimsek.web.util.HeaderUtil;
import com.hazelcast.meliksahsimsek.web.util.ResponseUtil;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing {@link com.hazelcast.meliksahsimsek.domain.Deployment}.
 */
@RestController
@RequestMapping("/api")
public class DeploymentResource {

    private static final String ENTITY_NAME = "deployment";
    private final Logger log = LoggerFactory.getLogger(DeploymentResource.class);
    private final DeploymentRepository deploymentRepository;
    private final KubernetesService kubernetesService;
    private final DeploymentMapper deploymentMapper;
    @Value("${application.clientApp.name}")
    private String applicationName;
    private DeploymentService deploymentService;

    public DeploymentResource(DeploymentRepository deploymentRepository, KubernetesService kubernetesService,
            DeploymentMapper deploymentMapper, DeploymentService deploymentService) {
        this.deploymentRepository = deploymentRepository;
        this.kubernetesService = kubernetesService;
        this.deploymentMapper = deploymentMapper;
        this.deploymentService = deploymentService;
    }

    /**
     * {@code POST  /deployments} : Create a new deployment.
     *
     * @param deployment the deployment to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new deployment, or with
     * status {@code 400 (Bad Request)} if the deployment has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/deployments")
    public ResponseEntity<Deployment> createDeployment(@RequestBody Deployment deployment) throws URISyntaxException {
        log.debug("REST request to save Deployment : {}", deployment);
        if (deployment.getId() != null) {
            throw new BadRequestAlertException("A new deployment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Deployment result = kubernetesService.createOrUpdateDeployment(deployment);

        return ResponseEntity.created(new URI("/api/deployments/" + result.getId()))
                .headers(HeaderUtil
                        .createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * {@code PUT  /deployments} : Updates an existing deployment.
     *
     * @param deployment the deployment to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated deployment, or with
     * status {@code 400 (Bad Request)} if the deployment is not valid, or with status {@code 500 (Internal Server
     * Error)} if the deployment couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/deployments")
    public ResponseEntity<Deployment> updateDeployment(@RequestBody Deployment deployment) throws URISyntaxException {
        log.debug("REST request to update Deployment : {}", deployment);
        if (deployment.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        kubernetesService.createOrUpdateDeployment(deployment);
        Deployment result = deploymentRepository
                .findByMetadataNameEqualsAndNamespace_NameEquals(deployment.getMetadataName(),
                        deployment.getNamespace().getName())
                .orElseThrow(NoSuchElementException::new);
        return ResponseEntity.ok()
                .headers(HeaderUtil
                        .createEntityUpdateAlert(applicationName, false, ENTITY_NAME, deployment.getId().toString()))
                .body(result);
    }

    /**
     * {@code GET  /deployments} : get all the deployments.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of deployments in body.
     */
    @GetMapping("/deployments")
    public List<Deployment> getAllDeployments() {
        log.debug("REST request to get all Deployments");
        kubernetesService.getAndUpdateDeployments();
        List<Deployment> deployments = deploymentService.findAllDeploymentsEager();
        return deployments;
    }

    /**
     * {@code GET  /deployments} : get all the deployments.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of deployments in body.
     */
    @GetMapping("/namespaces/{namespace}/deployments")
    public List<Deployment> getAllDeployments(@PathVariable String namespace) {
        log.debug("REST request to get all Deployments");
        kubernetesService.getAndUpdateDeployments(namespace);
        List<Deployment> deployments = deploymentRepository.findAllByNamespace_Name(namespace);
        return deployments;
    }

    /**
     * {@code GET  /deployments/:id} : get the "id" deployment.
     *
     * @param id the id of the deployment to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the deployment, or with status
     * {@code 404 (Not Found)}.
     */
    @GetMapping("/deployments/{id}")
    public ResponseEntity<Deployment> getDeploymentById(@PathVariable Long id) {
        log.debug("REST request to get Deployment : {}", id);
        Optional<Deployment> deployment = deploymentRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(kubernetesService.getDeployment(deployment.orElse(null)));
    }

    /**
     * {@code GET  /deployments/:id} : get the "id" deployment.
     *
     * @param metadataName the name of the deployment metadata to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the deployment, or with status
     * {@code 404 (Not Found)}.
     */
    @GetMapping("/namespaces/{namespace}/deployments/{metadataName}")
    public ResponseEntity<Deployment> getDeploymentByMetadataName(@PathVariable String namespace,
            @PathVariable String metadataName) {
        log.debug("REST request to get Deployment : {}", metadataName);

        final Optional<Deployment> deployment = deploymentRepository
                .findByMetadataNameEqualsAndNamespace_NameEquals(metadataName, namespace);
        return ResponseUtil
                .wrapOrNotFound(kubernetesService.getDeployment(deployment.orElseThrow(NoSuchElementException::new)));
    }

    /**
     * {@code DELETE  /deployments/:id} : delete the "id" deployment.
     *
     * @param id the id of the deployment to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/deployments/{id}")
    public ResponseEntity<Void> deleteDeployment(@PathVariable Long id) {
        log.debug("REST request to delete Deployment : {}", id);
        Deployment deployment = deploymentRepository.findById(id).orElseThrow(NoSuchElementException::new);
        kubernetesService.deleteDeployment(deployment);
        return ResponseEntity.noContent().headers(
                HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code DELETE  /deployments/:id} : delete the "id" deployment.
     *
     * @param metadataName the name of the deployment metadata to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/namespaces/{namespace}/deployments/{metadataName}")
    public ResponseEntity<Void> deleteDeployment(@PathVariable String namespace, @PathVariable String metadataName) {
        log.debug("REST request to delete Deployment : {}", metadataName);
        Deployment deployment = deploymentRepository
                .findByMetadataNameEqualsAndNamespace_NameEquals(metadataName, namespace)
                .orElseThrow(NoSuchElementException::new);
        kubernetesService.deleteDeployment(deployment);
        return ResponseEntity.noContent().headers(
                HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, metadataName)).build();
    }
}
