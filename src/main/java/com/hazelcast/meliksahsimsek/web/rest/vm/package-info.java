/**
 * View Models used by Spring MVC REST controllers.
 */
package com.hazelcast.meliksahsimsek.web.rest.vm;
