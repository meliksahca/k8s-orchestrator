package com.hazelcast.meliksahsimsek.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hazelcast.meliksahsimsek.domain.util.HashCodeUtil;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * A Port.
 */
@Entity
@Table(name = "port")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Port implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "port")
    private String port;

    @ManyToOne
    @JsonIgnore
    @NotFound(action = NotFoundAction.IGNORE)
    private Container container;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPort() {
        return port;
    }

    public Port port(String port) {
        this.port = port;
        return this;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public Container getContainer() {
        return container;
    }

    public Port container(Container container) {
        this.container = container;
        return this;
    }

    public void setContainer(Container container) {
        this.container = container;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Port)) {
            return false;
        }
        return port != null && port.equals(((Port) o).port)
                && container != null && container.equals(((Port) o).container);
    }

    @Override
    public int hashCode() {
        return HashCodeUtil.calculateHashCode(port, container);
    }

    @Override
    public String toString() {
        return "Port{" +
            "id=" + getId() +
            ", port='" + getPort() + "'" +
            "}";
    }
}
