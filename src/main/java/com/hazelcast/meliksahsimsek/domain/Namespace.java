package com.hazelcast.meliksahsimsek.domain;

import com.hazelcast.meliksahsimsek.domain.util.HashCodeUtil;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Namespace.
 */
@Entity
@Table(name = "namespace")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Namespace implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", unique = true)
    private String name;

    @OneToMany(mappedBy = "namespace", cascade = CascadeType.DETACH, fetch= FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Deployment> deployments = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Namespace name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Deployment> getDeployments() {
        return deployments;
    }

    public Namespace deployments(Set<Deployment> deployments) {
        this.deployments = deployments;
        return this;
    }

    public Namespace addDeployment(Deployment deployment) {
        this.deployments.add(deployment);
        deployment.setNamespace(this);
        return this;
    }

    public Namespace removeDeployment(Deployment deployment) {
        this.deployments.remove(deployment);
        deployment.setNamespace(null);
        return this;
    }

    public void setDeployments(Set<Deployment> deployments) {
        this.deployments = deployments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Namespace)) {
            return false;
        }
        return name != null && name.equals(((Namespace) o).name);
    }

    @Override
    public int hashCode() {
        return HashCodeUtil.calculateHashCode(name);
    }

    @Override
    public String toString() {
        return "Namespace{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
