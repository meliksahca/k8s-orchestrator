package com.hazelcast.meliksahsimsek.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.hazelcast.meliksahsimsek.domain.util.HashCodeUtil;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.CascadeType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Deployment.
 */
@Entity
@Table(name = "deployment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Deployment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "api_version")
    private String apiVersion;

    @Column(name = "kind")
    private String kind;

    @Column(name = "metadata_name")
    private String metadataName;

    @Column(name = "label_key")
    private String labelKey;

    @Column(name = "label_value")
    private String labelValue;

    @Column(name = "replica")
    private Integer replica;

    @OneToMany(mappedBy = "deployment", cascade = CascadeType.REMOVE, orphanRemoval = true, fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Container> containers = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("deployments")
    private Namespace namespace;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public Deployment apiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
        return this;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Deployment kind(String kind) {
        this.kind = kind;
        return this;
    }

    public String getMetadataName() {
        return metadataName;
    }

    public void setMetadataName(String metadataName) {
        this.metadataName = metadataName;
    }

    public Deployment metadataName(String metadataName) {
        this.metadataName = metadataName;
        return this;
    }

    public String getLabelKey() {
        return labelKey;
    }

    public void setLabelKey(String labelKey) {
        this.labelKey = labelKey;
    }

    public Deployment labelKey(String labelKey) {
        this.labelKey = labelKey;
        return this;
    }

    public String getLabelValue() {
        return labelValue;
    }

    public void setLabelValue(String labelValue) {
        this.labelValue = labelValue;
    }

    public Deployment labelValue(String labelValue) {
        this.labelValue = labelValue;
        return this;
    }

    public Integer getReplica() {
        return replica;
    }

    public void setReplica(Integer replica) {
        this.replica = replica;
    }

    public Deployment replica(Integer replica) {
        this.replica = replica;
        return this;
    }

    public Set<Container> getContainers() {
        return containers;
    }

    public void setContainers(Set<Container> containers) {
        this.containers = containers;
    }

    public Deployment containers(Set<Container> containers) {
        this.containers = containers;
        return this;
    }

    public Deployment addContainer(Container container) {
        this.containers.add(container);
        container.setDeployment(this);
        return this;
    }

    public Deployment removeContainer(Container container) {
        this.containers.remove(container);
        container.setDeployment(null);
        return this;
    }

    public Namespace getNamespace() {
        return namespace;
    }

    public void setNamespace(Namespace namespace) {
        this.namespace = namespace;
    }

    public Deployment namespace(Namespace namespace) {
        this.namespace = namespace;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Deployment)) {
            return false;
        }
        return metadataName != null && metadataName.equals(((Deployment) o).metadataName)
                && namespace != null && namespace.getName() != null && namespace.getName()
                .equals(((Deployment) o).namespace.getName());
    }

    @Override
    public int hashCode() {
        return HashCodeUtil.calculateHashCode(metadataName, namespace);
    }

    @Override
    public String toString() {
        return "Deployment{" +
                "id=" + getId() +
                ", apiVersion='" + getApiVersion() + "'" +
                ", kind='" + getKind() + "'" +
                ", metadataName='" + getMetadataName() + "'" +
                ", labelKey='" + getLabelKey() + "'" +
                ", labelValue='" + getLabelValue() + "'" +
                ", replica=" + getReplica() +
                "}";
    }
}
