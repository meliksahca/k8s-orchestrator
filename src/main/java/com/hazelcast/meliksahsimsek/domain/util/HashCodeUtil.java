package com.hazelcast.meliksahsimsek.domain.util;

import java.util.Arrays;
import java.util.Objects;

public class HashCodeUtil {
    public static int calculateHashCode(Object... objects) {
        int hashCode = 31;
        hashCode *= Arrays.stream(objects).filter(Objects::nonNull).mapToInt(Object::hashCode)
                .reduce(1, (a, b) -> a * b);
        return hashCode;
    }
}
