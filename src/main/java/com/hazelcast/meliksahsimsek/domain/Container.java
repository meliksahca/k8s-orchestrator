package com.hazelcast.meliksahsimsek.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hazelcast.meliksahsimsek.domain.util.HashCodeUtil;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.CascadeType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * A Container.
 */
@Entity
@Table(name = "container")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Container implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "image")
    private String image;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "container", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch=FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Port> ports = new HashSet<>();

    @ManyToOne
    @JsonIgnore
    @NotFound(action = NotFoundAction.IGNORE)
    private Deployment deployment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public Container image(String image) {
        this.image = image;
        return this;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public Container name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Port> getPorts() {
        return ports;
    }

    public Container ports(Set<Port> ports) {
        this.ports = ports;
        return this;
    }

    public Container addPort(Port port) {
        this.ports.add(port);
        port.setContainer(this);
        return this;
    }

    public Container removePort(Port port) {
        this.ports.remove(port);
        port.setContainer(null);
        return this;
    }

    public void setPorts(Set<Port> ports) {
        this.ports = ports;
    }

    public Deployment getDeployment() {
        return deployment;
    }

    public Container deployment(Deployment deployment) {
        this.deployment = deployment;
        return this;
    }

    public void setDeployment(Deployment deployment) {
        this.deployment = deployment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Container)) {
            return false;
        }
        return name != null && name.equals(((Container) o).name)
                && image != null && image.equals(((Container) o).image)
                && deployment != null && deployment.equals(((Container)o).deployment);
    }

    @Override
    public int hashCode() {
        return HashCodeUtil.calculateHashCode(name, image, deployment);
    }

    @Override
    public String toString() {
        return "Container{" +
            "id=" + getId() +
            ", image='" + getImage() + "'" +
            ", name='" + getName() + "'" +
            "}";
    }
}
