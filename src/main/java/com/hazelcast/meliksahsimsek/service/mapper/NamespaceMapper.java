package com.hazelcast.meliksahsimsek.service.mapper;

import com.hazelcast.meliksahsimsek.domain.Namespace;
import io.fabric8.kubernetes.api.model.NamespaceBuilder;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class NamespaceMapper {
    public Namespace namespaceToNamespaceEntity(io.fabric8.kubernetes.api.model.Namespace namespace) {
        Namespace namespaceEntity = new Namespace();
        namespaceEntity.setName(namespace.getMetadata().getName());
        return namespaceEntity;
    }

    public List<Namespace> namespacesToNamespaceEntities(List<io.fabric8.kubernetes.api.model.Namespace> namespaces) {
        return namespaces.stream().map(this::namespaceToNamespaceEntity).collect(Collectors.toList());
    }

    public io.fabric8.kubernetes.api.model.Namespace namespaceEntityToNamespace(Namespace namespaceEntity) {
        NamespaceBuilder namespaceBuilder = new NamespaceBuilder();
        namespaceBuilder.withNewMetadata().withName(namespaceEntity.getName()).endMetadata();
        return namespaceBuilder.build();
    }

    public List<io.fabric8.kubernetes.api.model.Namespace> namespaceEntitiesToNamespaces(List<Namespace> namespaceEntities) {
        return namespaceEntities.stream().map(this::namespaceEntityToNamespace).collect(Collectors.toList());
    }
}
