package com.hazelcast.meliksahsimsek.service.mapper;

import com.hazelcast.meliksahsimsek.domain.Namespace;
import com.hazelcast.meliksahsimsek.domain.Port;
import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.ContainerBuilder;
import io.fabric8.kubernetes.api.model.ContainerPort;
import io.fabric8.kubernetes.api.model.ContainerPortBuilder;
import io.fabric8.kubernetes.api.model.apps.Deployment;
import io.fabric8.kubernetes.api.model.apps.DeploymentBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class DeploymentMapper {

    public Deployment deploymentEntityToDeployment(com.hazelcast.meliksahsimsek.domain.Deployment deployment) {
        DeploymentBuilder deploymentBuilder = new DeploymentBuilder();
        List<Container> containerList = new ArrayList<>();
        Map<String, String> label = new HashMap<>();
        label.put(deployment.getLabelKey(), deployment.getLabelValue());
        deployment.getContainers().forEach(container -> {
            List<ContainerPort> ports = new ArrayList<>();
            container.getPorts().forEach(port -> {
                ContainerPortBuilder containerPortBuilder = new ContainerPortBuilder();
                containerPortBuilder.withContainerPort(Integer.valueOf(port.getPort()));
                ports.add(containerPortBuilder.build());
            });
            ContainerBuilder containerBuilder = new ContainerBuilder();
            containerBuilder.withName(container.getName())
                    .withImage(container.getImage())
                    .withPorts(ports);
            containerList.add(containerBuilder.build());
        });
        deploymentBuilder.withApiVersion(deployment.getApiVersion())
                .withKind(deployment.getKind())
                .withNewMetadata()
                .withName(deployment.getMetadataName())
                .withNamespace(deployment.getNamespace().getName())
                .withLabels(label)
                .endMetadata()
                .withNewSpec()
                .withReplicas(deployment.getReplica())
                .withNewSelector()
                .withMatchLabels(label)
                .endSelector()
                .withNewTemplate()
                .withNewMetadata()
                .withLabels(label)
                .endMetadata()
                .withNewSpec()
                .withContainers(containerList)
                .endSpec()
                .endTemplate()
                .endSpec();
        return deploymentBuilder.build();
    }

    public com.hazelcast.meliksahsimsek.domain.Deployment deploymentToDeploymentEntity(Deployment deployment) {
        com.hazelcast.meliksahsimsek.domain.Deployment deploymentEntity = new com.hazelcast.meliksahsimsek.domain.Deployment();
        Namespace namespace = new Namespace();
        namespace.setName(deployment.getMetadata().getNamespace());
        deploymentEntity.setContainers(deployment.getSpec().getTemplate().getSpec().getContainers().stream().filter(Objects::nonNull)
                .map(container -> {
                    com.hazelcast.meliksahsimsek.domain.Container containerEntity = new com.hazelcast.meliksahsimsek.domain.Container();
                    containerEntity.setImage(container.getImage());
                    containerEntity.setName(container.getName());
                    containerEntity.setDeployment(deploymentEntity);
                    containerEntity.setPorts(container.getPorts().stream().filter(Objects::nonNull).map(port -> {
                        Port portEntity = new Port();
                        portEntity.setContainer(containerEntity);
                        portEntity.setPort(String.valueOf(port.getContainerPort()));
                        return portEntity;
                    }).collect(Collectors.toSet()));
                    return containerEntity;
                }).collect(Collectors.toSet()));
        deploymentEntity.setNamespace(namespace);
        deploymentEntity.setApiVersion(deployment.getApiVersion());
        deploymentEntity.setKind(deployment.getKind());
        deploymentEntity.setLabelKey(deployment.getMetadata().getLabels().keySet().iterator().next());
        deploymentEntity.setLabelValue(deployment.getMetadata().getLabels().values().iterator().next());
        deploymentEntity.setMetadataName(deployment.getMetadata().getName());
        deploymentEntity.setReplica(deployment.getSpec().getReplicas());
        return deploymentEntity;
    }

    public List<com.hazelcast.meliksahsimsek.domain.Deployment> deploymentsToDeploymentEntities(
            List<Deployment> deployments) {
        return deployments.stream()
                .filter(Objects::nonNull)
                .map(this::deploymentToDeploymentEntity)
                .collect(Collectors.toList());
    }

    public List<Deployment> deploymentEntitiesToDeployments(
            List<com.hazelcast.meliksahsimsek.domain.Deployment> deployments) {
        return deployments.stream()
                .filter(Objects::nonNull)
                .map(this::deploymentEntityToDeployment)
                .collect(Collectors.toList());
    }
}
