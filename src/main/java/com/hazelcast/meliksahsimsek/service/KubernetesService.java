package com.hazelcast.meliksahsimsek.service;

import com.hazelcast.meliksahsimsek.domain.Container;
import com.hazelcast.meliksahsimsek.domain.Deployment;
import com.hazelcast.meliksahsimsek.domain.Namespace;
import com.hazelcast.meliksahsimsek.repository.ContainerRepository;
import com.hazelcast.meliksahsimsek.repository.DeploymentRepository;
import com.hazelcast.meliksahsimsek.repository.NamespaceRepository;
import com.hazelcast.meliksahsimsek.repository.PortRepository;
import com.hazelcast.meliksahsimsek.service.mapper.DeploymentMapper;
import com.hazelcast.meliksahsimsek.service.mapper.NamespaceMapper;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.KubernetesClient;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class KubernetesService {

    @Autowired
    DeploymentMapper deploymentMapper;
    @Autowired
    NamespaceMapper namespaceMapper;

    private Config kubernetesClientConfig;
    private KubernetesClient kubernetesClient;
    private DeploymentRepository deploymentRepository;
    private NamespaceRepository namespaceRepository;
    private ContainerRepository containerRepository;
    private PortRepository portRepository;

    public KubernetesService(KubernetesClient kubernetesClient, Config kubernetesClientConfig,
            DeploymentRepository deploymentRepository, NamespaceRepository namespaceRepository,
            ContainerRepository containerRepository, PortRepository portRepository) {
        this.kubernetesClient = kubernetesClient;
        this.kubernetesClientConfig = kubernetesClientConfig;
        this.deploymentRepository = deploymentRepository;
        this.namespaceRepository = namespaceRepository;
        this.containerRepository = containerRepository;
        this.portRepository = portRepository;
    }

    public List<Namespace> getAndUpdateNamespaces() {
        List<io.fabric8.kubernetes.api.model.Namespace> namespaces = kubernetesClient.namespaces().list().getItems();
        List<Namespace> namespaceEntities = namespaceMapper.namespacesToNamespaceEntities(namespaces);
        namespaceRepository.saveAllByName(namespaceEntities);
        List<com.hazelcast.meliksahsimsek.domain.Namespace> persistedEntities = namespaceRepository.findAll();
        persistedEntities.stream().filter(persistedEntity -> !namespaceEntities.contains(persistedEntity))
                .forEach(deletedNamespace -> {
                    namespaceRepository.deleteByName(deletedNamespace.getName());
                });
        return namespaceEntities;
    }

    public Optional<Namespace> getAndUpdateNamespace(String name) {
        io.fabric8.kubernetes.api.model.Namespace namespace = kubernetesClient.namespaces().withName(name).get();
        Namespace namespaceEntitiy = null;
        if (Objects.nonNull(namespace)) {
            namespaceEntitiy = namespaceMapper.namespaceToNamespaceEntity(namespace);
            namespaceRepository.saveByName(namespaceEntitiy);
        } else {
            namespaceRepository.findByNameEquals(name).ifPresent(namespace1 -> {
                namespaceRepository.deleteByName(namespace1.getName());
            });
        }
        return Optional.ofNullable(namespaceEntitiy);
    }

    public Optional<Namespace> deleteNamespace(String namespace) {
        Optional<Namespace> namespaceEntity = getAndUpdateNamespace(namespace);
        namespaceEntity.ifPresent(namespacePresented -> {
            namespaceRepository.deleteByName(namespace);
            deploymentRepository.deleteAll(namespacePresented.getDeployments());
            List<Container> containers = namespacePresented.getDeployments().stream().flatMap(deployment ->
                    deployment.getContainers().stream()).collect(Collectors.toList());
            containerRepository.deleteAll(containers);
            portRepository.deleteAll(containers.stream().flatMap(container -> container.getPorts().stream())
                    .collect(Collectors.toList()));
            kubernetesClient.namespaces().delete(namespaceMapper.namespaceEntityToNamespace(namespacePresented));
        });

        return namespaceEntity;
    }

    public Namespace createOrUpdateNamespace(Namespace namespaceEntity) {
        kubernetesClient.namespaces().createOrReplace(namespaceMapper.namespaceEntityToNamespace(namespaceEntity));
        com.hazelcast.meliksahsimsek.domain.Namespace persistedEntity = namespaceRepository
                .saveByName(namespaceEntity);
        return persistedEntity;
    }

    public List<Deployment> getAndUpdateDeployments(String namespace) {
        return getAndUpdateDeployments(kubernetesClient.apps().deployments().inNamespace(namespace).list().getItems(),
                deploymentRepository.findAllByNamespace_Name(namespace));
    }

    public List<Deployment> getAndUpdateDeployments() {
        return getAndUpdateDeployments(kubernetesClient.apps().deployments().inAnyNamespace().list().getItems(),
                deploymentRepository.findAll());
    }

    private List<Deployment> getAndUpdateDeployments(List<io.fabric8.kubernetes.api.model.apps.Deployment> deployments,
            List<Deployment> persistedDeployments) {
        return saveAllCascaded(deployments, persistedDeployments);
    }

    private com.hazelcast.meliksahsimsek.domain.Deployment saveCascaded(Deployment deployment) {
        com.hazelcast.meliksahsimsek.domain.Namespace namespace = deployment.getNamespace();
        Set<Container> containerSet = deployment.getContainers();
        namespaceRepository.saveByName(namespace);
        namespaceRepository.findByNameEquals(deployment.getNamespace().getName())
                .ifPresent(deployment::setNamespace);

        Deployment result = deploymentRepository.saveByMetadataNameAndNamspace_Name(deployment);
        containerSet.forEach(container -> container.setDeployment(result));
        List<Container> persistedContainers = containerRepository.saveAllByNameAndImage(containerSet);
        persistedContainers.forEach(persistedContainer -> {
            persistedContainer.getPorts().forEach(port -> {
                port.setContainer(persistedContainer);
            });
            portRepository.saveAll(persistedContainer.getPorts());
        });
        return result;
    }

    private List<Deployment> saveAllCascaded(List<io.fabric8.kubernetes.api.model.apps.Deployment> deployments,
            List<Deployment> persistedDeployments) {
        List<Deployment> results = persistedDeployments.stream().map(this::saveCascaded).collect(Collectors.toList());
        List<com.hazelcast.meliksahsimsek.domain.Deployment> kubernetesPersistedDeployments = deploymentMapper
                .deploymentsToDeploymentEntities(deployments);
        persistedDeployments.stream().filter(deployment -> !kubernetesPersistedDeployments.contains(deployment))
                .forEach(deployment -> deploymentRepository
                        .deleteByMetadataNameAndNamespace_Name(deployment.getMetadataName(),
                                deployment.getNamespace().getName()));
        return results;
    }

    public Optional<Deployment> getDeployment(Deployment deployment) {
        List<io.fabric8.kubernetes.api.model.apps.Deployment> deployments = kubernetesClient.apps().deployments()
                .inNamespace(deployment.getNamespace().getName())
                .withField("metadata.name", deployment.getMetadataName()).list().getItems();
        io.fabric8.kubernetes.api.model.apps.Deployment kubeDeployment =
                deployments.size() == 0 ? null : deployments.get(0);
        Deployment result = null;
        if (Objects.nonNull(kubeDeployment)) {
            result = saveCascaded(deployment);
        } else {
            com.hazelcast.meliksahsimsek.domain.Deployment deploymentPersisted = deploymentRepository
                    .findByMetadataNameEqualsAndNamespace_NameEquals(deployment.getMetadataName(),
                            deployment.getNamespace().getName())
                    .orElseThrow(NoSuchElementException::new);
            portRepository.deleteAll(
                    deploymentPersisted.getContainers().stream().flatMap(container -> container.getPorts().stream())
                            .collect(Collectors.toList()));
            containerRepository.deleteAll(deploymentPersisted.getContainers());
            deploymentRepository.deleteByMetadataNameAndNamespace_Name(deployment.getMetadataName(),
                    deployment.getNamespace().getName());
        }
        return Optional.ofNullable(result);
    }

    public Optional<Deployment> deleteDeployment(Deployment deploymentEntity) {
        portRepository.deleteAll(
                deploymentEntity.getContainers().stream().flatMap(container -> container.getPorts().stream())
                        .collect(Collectors.toSet()));
        containerRepository.deleteAll(deploymentEntity.getContainers());
        deploymentRepository.deleteById(deploymentEntity.getId());
        kubernetesClient.apps().deployments().inNamespace(deploymentEntity.getNamespace().getName())
                .delete(deploymentMapper.deploymentEntityToDeployment(deploymentEntity));
        return Optional.ofNullable(deploymentEntity);
    }

    public com.hazelcast.meliksahsimsek.domain.Deployment createOrUpdateDeployment(
            com.hazelcast.meliksahsimsek.domain.Deployment deploymentEntity) {
        io.fabric8.kubernetes.api.model.apps.Deployment deployment = deploymentMapper
                .deploymentEntityToDeployment(deploymentEntity);
        kubernetesClient.apps().deployments().inNamespace(deploymentEntity.getNamespace().getName())
                .createOrReplace(deployment);
        return saveCascaded(deploymentEntity);
        /*if (deploymentEntity.getNamespace() != null) {
            deploymentEntity.setNamespace(namespaceRepository.saveByName(deploymentEntity.getNamespace()));
            deploymentRepository.findByMetadataNameEqualsAndNamespace_NameEquals(deploymentEntity.getMetadataName(),
                    deploymentEntity.getNamespace().getName())
                    .ifPresent(persistedEntity -> deploymentEntity.getContainers().stream().forEach(container -> {
                        container.setDeployment(persistedEntity);
                    }));
        }
        deploymentRepository.saveByMetadataNameAndNamspace_Name(deploymentEntity);
        containerRepository.saveAllByNameAndImage(deploymentEntity.getContainers());
        Set<Port> portSet = deploymentEntity.getContainers().stream()
                .flatMap(container -> container.getPorts().stream()).collect(Collectors.toSet());
        portSet.forEach(port -> {
            containerRepository
                    .findByNameAndAndImage(port.getContainer().getName(), port.getContainer().getImage())
                    .ifPresent(container -> {
                        port.setContainer(container);
                    });
        });
        portRepository.saveAllByPortAndContainerNameAndContainerImage(portSet);*/
    }
}
