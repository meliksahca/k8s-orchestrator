package com.hazelcast.meliksahsimsek.service;

import com.hazelcast.meliksahsimsek.domain.Deployment;
import com.hazelcast.meliksahsimsek.repository.ContainerRepository;
import com.hazelcast.meliksahsimsek.repository.DeploymentRepository;
import com.hazelcast.meliksahsimsek.repository.NamespaceRepository;
import com.hazelcast.meliksahsimsek.repository.PortRepository;
import com.hazelcast.meliksahsimsek.service.mapper.DeploymentMapper;
import com.hazelcast.meliksahsimsek.web.rest.DeploymentResource;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DeploymentService {

    private final Logger log = LoggerFactory.getLogger(DeploymentResource.class);
    private final DeploymentRepository deploymentRepository;
    private final DeploymentMapper deploymentMapper;
    @Value("${application.clientApp.name}")
    private String applicationName;
    private NamespaceRepository namespaceRepository;
    private ContainerRepository containerRepository;
    private PortRepository portRepository;

    public DeploymentService(DeploymentRepository deploymentRepository, DeploymentMapper deploymentMapper,
            NamespaceRepository namespaceRepository, ContainerRepository containerRepository,
            PortRepository portRepository) {
        this.deploymentRepository = deploymentRepository;
        this.deploymentMapper = deploymentMapper;
        this.namespaceRepository = namespaceRepository;
        this.containerRepository = containerRepository;
        this.portRepository = portRepository;
    }

    public List<Deployment> findAllDeploymentsEager() {
        List<Deployment> eagerDeploymentList = deploymentRepository.findAll();
        return eagerDeploymentList;
    }

}
