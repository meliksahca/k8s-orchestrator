import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { K8SOrchestratorSharedModule } from 'app/shared';

import {
  adminState,
  AuditsComponent,
  UserMgmtComponent,
  UserMgmtDetailComponent,
  UserMgmtUpdateComponent,
  UserMgmtDeleteDialogComponent,
  LogsComponent,
  HcmMetricsMonitoringComponent,
  HcmHealthModalComponent,
  HcmHealthCheckComponent,
  HcmConfigurationComponent,
  HcmDocsComponent
} from './';

@NgModule({
  imports: [K8SOrchestratorSharedModule, RouterModule.forChild(adminState)],
  declarations: [
    AuditsComponent,
    UserMgmtComponent,
    UserMgmtDetailComponent,
    UserMgmtUpdateComponent,
    UserMgmtDeleteDialogComponent,
    LogsComponent,
    HcmConfigurationComponent,
    HcmHealthCheckComponent,
    HcmHealthModalComponent,
    HcmDocsComponent,
    HcmMetricsMonitoringComponent
  ],
  entryComponents: [UserMgmtDeleteDialogComponent, HcmHealthModalComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class K8SOrchestratorAdminModule {}
