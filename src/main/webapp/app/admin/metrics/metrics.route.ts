import { Route } from '@angular/router';

import { HcmMetricsMonitoringComponent } from './metrics.component';

export const metricsRoute: Route = {
  path: 'hcm-metrics',
  component: HcmMetricsMonitoringComponent,
  data: {
    pageTitle: 'Application Metrics'
  }
};
