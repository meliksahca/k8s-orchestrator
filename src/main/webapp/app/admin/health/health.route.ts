import { Route } from '@angular/router';

import { HcmHealthCheckComponent } from './health.component';

export const healthRoute: Route = {
  path: 'hcm-health',
  component: HcmHealthCheckComponent,
  data: {
    pageTitle: 'Health Checks'
  }
};
