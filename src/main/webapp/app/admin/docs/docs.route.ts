import { Route } from '@angular/router';

import { HcmDocsComponent } from './docs.component';

export const docsRoute: Route = {
  path: 'docs',
  component: HcmDocsComponent,
  data: {
    pageTitle: 'API'
  }
};
