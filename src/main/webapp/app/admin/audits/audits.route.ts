import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Route } from '@angular/router';
import { HcmPaginationUtil, HcmResolvePagingParams } from '../../framework/public_api';

import { AuditsComponent } from './audits.component';

export const auditsRoute: Route = {
  path: 'audits',
  component: AuditsComponent,
  resolve: {
    pagingParams: HcmResolvePagingParams
  },
  data: {
    pageTitle: 'Audits',
    defaultSort: 'auditEventDate,desc'
  }
};
