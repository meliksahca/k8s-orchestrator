import { Route } from '@angular/router';

import { HcmConfigurationComponent } from './configuration.component';

export const configurationRoute: Route = {
  path: 'hcm-configuration',
  component: HcmConfigurationComponent,
  data: {
    pageTitle: 'Configuration'
  }
};
