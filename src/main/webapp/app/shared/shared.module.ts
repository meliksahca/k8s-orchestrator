import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { K8SOrchestratorSharedCommonModule, HcmLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [K8SOrchestratorSharedCommonModule],
  declarations: [HcmLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [HcmLoginModalComponent],
  exports: [K8SOrchestratorSharedCommonModule, HcmLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class K8SOrchestratorSharedModule {
  static forRoot() {
    return {
      ngModule: K8SOrchestratorSharedModule
    };
  }
}
