import { IContainer } from 'app/shared/model/container.model';

export interface IPort {
  id?: number;
  port?: string;
  container?: IContainer;
}

export class Port implements IPort {
  constructor(public id?: number, public port?: string, public container?: IContainer) {}
}
