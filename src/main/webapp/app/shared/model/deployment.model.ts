import { IContainer } from 'app/shared/model/container.model';
import { INamespace } from 'app/shared/model/namespace.model';

export interface IDeployment {
  id?: number;
  apiVersion?: string;
  kind?: string;
  metadataName?: string;
  labelKey?: string;
  labelValue?: string;
  replica?: number;
  containers?: IContainer[];
  namespace?: INamespace;
}

export class Deployment implements IDeployment {
  constructor(
    public id?: number,
    public apiVersion?: string,
    public kind?: string,
    public metadataName?: string,
    public labelKey?: string,
    public labelValue?: string,
    public replica?: number,
    public containers?: IContainer[],
    public namespace?: INamespace
  ) {}
}
