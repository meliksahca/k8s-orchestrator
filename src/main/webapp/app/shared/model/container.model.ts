import { IPort } from 'app/shared/model/port.model';
import { IDeployment } from 'app/shared/model/deployment.model';

export interface IContainer {
  id?: number;
  image?: string;
  name?: string;
  ports?: IPort[];
  deployment?: IDeployment;
}

export class Container implements IContainer {
  constructor(public id?: number, public image?: string, public name?: string, public ports?: IPort[], public deployment?: IDeployment) {}
}
