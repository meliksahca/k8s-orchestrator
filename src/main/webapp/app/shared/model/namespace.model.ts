import { IDeployment } from 'app/shared/model/deployment.model';

export interface INamespace {
  id?: number;
  name?: string;
  deployments?: IDeployment[];
}

export class Namespace implements INamespace {
  constructor(public id?: number, public name?: string, public deployments?: IDeployment[]) {}
}
