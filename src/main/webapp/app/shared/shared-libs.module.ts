import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgHcmFrameworkModule } from '../framework/public_api';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CookieModule } from 'ngx-cookie';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  imports: [NgbModule, InfiniteScrollModule, CookieModule.forRoot(), FontAwesomeModule, ReactiveFormsModule],
  exports: [FormsModule, CommonModule, NgbModule, NgHcmFrameworkModule, InfiniteScrollModule, FontAwesomeModule, ReactiveFormsModule]
})
export class K8SOrchestratorSharedLibsModule {
  static forRoot() {
    return {
      ngModule: K8SOrchestratorSharedLibsModule
    };
  }
}
