import { NgModule } from '@angular/core';

import { K8SOrchestratorSharedLibsModule, HcmAlertComponent, HcmAlertErrorComponent } from './';

@NgModule({
  imports: [K8SOrchestratorSharedLibsModule],
  declarations: [HcmAlertComponent, HcmAlertErrorComponent],
  exports: [K8SOrchestratorSharedLibsModule, HcmAlertComponent, HcmAlertErrorComponent]
})
export class K8SOrchestratorSharedCommonModule {}
