import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { INamespace } from 'app/shared/model/namespace.model';

type EntityResponseType = HttpResponse<INamespace>;
type EntityArrayResponseType = HttpResponse<INamespace[]>;

@Injectable({ providedIn: 'root' })
export class NamespaceService {
  public resourceUrl = SERVER_API_URL + 'api/namespaces';

  constructor(protected http: HttpClient) {}

  create(namespace: INamespace): Observable<EntityResponseType> {
    return this.http.post<INamespace>(this.resourceUrl, namespace, { observe: 'response' });
  }

  update(namespace: INamespace): Observable<EntityResponseType> {
    return this.http.put<INamespace>(this.resourceUrl, namespace, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<INamespace>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<INamespace[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
