import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { HcmEventManager, HcmAlertService } from 'app/framework/public_api';

import { INamespace } from 'app/shared/model/namespace.model';
import { AccountService } from 'app/core';
import { NamespaceService } from './namespace.service';

@Component({
  selector: 'hcm-namespace',
  templateUrl: './namespace.component.html'
})
export class NamespaceComponent implements OnInit, OnDestroy {
  namespaces: INamespace[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected namespaceService: NamespaceService,
    protected hcmAlertService: HcmAlertService,
    protected eventManager: HcmEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.namespaceService
      .query()
      .pipe(
        filter((res: HttpResponse<INamespace[]>) => res.ok),
        map((res: HttpResponse<INamespace[]>) => res.body)
      )
      .subscribe(
        (res: INamespace[]) => {
          this.namespaces = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInNamespaces();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: INamespace) {
    return item.id;
  }

  registerChangeInNamespaces() {
    this.eventSubscriber = this.eventManager.subscribe('namespaceListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.hcmAlertService.error(errorMessage, null, null);
  }
}
