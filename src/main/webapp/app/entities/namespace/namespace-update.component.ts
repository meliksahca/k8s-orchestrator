import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { INamespace, Namespace } from 'app/shared/model/namespace.model';
import { NamespaceService } from './namespace.service';

@Component({
  selector: 'hcm-namespace-update',
  templateUrl: './namespace-update.component.html'
})
export class NamespaceUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: []
  });

  constructor(protected namespaceService: NamespaceService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ namespace }) => {
      this.updateForm(namespace);
    });
  }

  updateForm(namespace: INamespace) {
    this.editForm.patchValue({
      id: namespace.id,
      name: namespace.name
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const namespace = this.createFromForm();
    if (namespace.id !== undefined) {
      this.subscribeToSaveResponse(this.namespaceService.update(namespace));
    } else {
      this.subscribeToSaveResponse(this.namespaceService.create(namespace));
    }
  }

  private createFromForm(): INamespace {
    return {
      ...new Namespace(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INamespace>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
