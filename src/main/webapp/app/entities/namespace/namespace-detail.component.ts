import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { INamespace } from 'app/shared/model/namespace.model';

@Component({
  selector: 'hcm-namespace-detail',
  templateUrl: './namespace-detail.component.html'
})
export class NamespaceDetailComponent implements OnInit {
  namespace: INamespace;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ namespace }) => {
      this.namespace = namespace;
    });
  }

  previousState() {
    window.history.back();
  }
}
