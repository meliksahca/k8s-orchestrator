import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HcmEventManager } from 'app/framework/public_api';

import { INamespace } from 'app/shared/model/namespace.model';
import { NamespaceService } from './namespace.service';

@Component({
  selector: 'hcm-namespace-delete-dialog',
  templateUrl: './namespace-delete-dialog.component.html'
})
export class NamespaceDeleteDialogComponent {
  namespace: INamespace;

  constructor(protected namespaceService: NamespaceService, public activeModal: NgbActiveModal, protected eventManager: HcmEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.namespaceService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'namespaceListModification',
        content: 'Deleted an namespace'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'hcm-namespace-delete-popup',
  template: ''
})
export class NamespaceDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ namespace }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(NamespaceDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.namespace = namespace;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/namespace', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/namespace', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
