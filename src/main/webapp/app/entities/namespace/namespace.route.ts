import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Namespace } from 'app/shared/model/namespace.model';
import { NamespaceService } from './namespace.service';
import { NamespaceComponent } from './namespace.component';
import { NamespaceDetailComponent } from './namespace-detail.component';
import { NamespaceUpdateComponent } from './namespace-update.component';
import { NamespaceDeletePopupComponent } from './namespace-delete-dialog.component';
import { INamespace } from 'app/shared/model/namespace.model';

@Injectable({ providedIn: 'root' })
export class NamespaceResolve implements Resolve<INamespace> {
  constructor(private service: NamespaceService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<INamespace> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Namespace>) => response.ok),
        map((namespace: HttpResponse<Namespace>) => namespace.body)
      );
    }
    return of(new Namespace());
  }
}

export const namespaceRoute: Routes = [
  {
    path: '',
    component: NamespaceComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Namespaces'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: NamespaceDetailComponent,
    resolve: {
      namespace: NamespaceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Namespaces'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: NamespaceUpdateComponent,
    resolve: {
      namespace: NamespaceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Namespaces'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: NamespaceUpdateComponent,
    resolve: {
      namespace: NamespaceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Namespaces'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const namespacePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: NamespaceDeletePopupComponent,
    resolve: {
      namespace: NamespaceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Namespaces'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
