import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { K8SOrchestratorSharedModule } from 'app/shared';
import {
  NamespaceComponent,
  NamespaceDetailComponent,
  NamespaceUpdateComponent,
  NamespaceDeletePopupComponent,
  NamespaceDeleteDialogComponent,
  namespaceRoute,
  namespacePopupRoute
} from './';

const ENTITY_STATES = [...namespaceRoute, ...namespacePopupRoute];

@NgModule({
  imports: [K8SOrchestratorSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    NamespaceComponent,
    NamespaceDetailComponent,
    NamespaceUpdateComponent,
    NamespaceDeleteDialogComponent,
    NamespaceDeletePopupComponent
  ],
  entryComponents: [NamespaceComponent, NamespaceUpdateComponent, NamespaceDeleteDialogComponent, NamespaceDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class K8SOrchestratorNamespaceModule {}
