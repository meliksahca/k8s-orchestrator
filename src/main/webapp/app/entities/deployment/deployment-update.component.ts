import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { HcmAlertService } from 'app/framework/public_api';
import { IDeployment, Deployment } from 'app/shared/model/deployment.model';
import { DeploymentService } from './deployment.service';
import { INamespace } from 'app/shared/model/namespace.model';
import { NamespaceService } from 'app/entities/namespace';

@Component({
  selector: 'hcm-deployment-update',
  templateUrl: './deployment-update.component.html'
})
export class DeploymentUpdateComponent implements OnInit {
  isSaving: boolean;

  namespaces: INamespace[];

  editForm = this.fb.group({
    id: [],
    apiVersion: [],
    kind: [],
    metadataName: [],
    labelKey: [],
    labelValue: [],
    replica: [],
    namespace: []
  });

  constructor(
    protected hcmAlertService: HcmAlertService,
    protected deploymentService: DeploymentService,
    protected namespaceService: NamespaceService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ deployment }) => {
      this.updateForm(deployment);
    });
    this.namespaceService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<INamespace[]>) => mayBeOk.ok),
        map((response: HttpResponse<INamespace[]>) => response.body)
      )
      .subscribe((res: INamespace[]) => (this.namespaces = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(deployment: IDeployment) {
    this.editForm.patchValue({
      id: deployment.id,
      apiVersion: deployment.apiVersion,
      kind: deployment.kind,
      metadataName: deployment.metadataName,
      labelKey: deployment.labelKey,
      labelValue: deployment.labelValue,
      replica: deployment.replica,
      namespace: deployment.namespace
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const deployment = this.createFromForm();
    if (deployment.id !== undefined) {
      this.subscribeToSaveResponse(this.deploymentService.update(deployment));
    } else {
      this.subscribeToSaveResponse(this.deploymentService.create(deployment));
    }
  }

  private createFromForm(): IDeployment {
    return {
      ...new Deployment(),
      id: this.editForm.get(['id']).value,
      apiVersion: this.editForm.get(['apiVersion']).value,
      kind: this.editForm.get(['kind']).value,
      metadataName: this.editForm.get(['metadataName']).value,
      labelKey: this.editForm.get(['labelKey']).value,
      labelValue: this.editForm.get(['labelValue']).value,
      replica: this.editForm.get(['replica']).value,
      namespace: this.editForm.get(['namespace']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDeployment>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.hcmAlertService.error(errorMessage, null, null);
  }

  trackNamespaceById(index: number, item: INamespace) {
    return item.id;
  }
}
