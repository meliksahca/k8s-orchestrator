import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { K8SOrchestratorSharedModule } from 'app/shared';
import {
  DeploymentComponent,
  DeploymentDetailComponent,
  DeploymentUpdateComponent,
  DeploymentDeletePopupComponent,
  DeploymentDeleteDialogComponent,
  deploymentRoute,
  deploymentPopupRoute
} from './';

const ENTITY_STATES = [...deploymentRoute, ...deploymentPopupRoute];

@NgModule({
  imports: [K8SOrchestratorSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    DeploymentComponent,
    DeploymentDetailComponent,
    DeploymentUpdateComponent,
    DeploymentDeleteDialogComponent,
    DeploymentDeletePopupComponent
  ],
  entryComponents: [DeploymentComponent, DeploymentUpdateComponent, DeploymentDeleteDialogComponent, DeploymentDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class K8SOrchestratorDeploymentModule {}
