import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HcmEventManager } from 'app/framework/public_api';

import { IDeployment } from 'app/shared/model/deployment.model';
import { DeploymentService } from './deployment.service';

@Component({
  selector: 'hcm-deployment-delete-dialog',
  templateUrl: './deployment-delete-dialog.component.html'
})
export class DeploymentDeleteDialogComponent {
  deployment: IDeployment;

  constructor(
    protected deploymentService: DeploymentService,
    public activeModal: NgbActiveModal,
    protected eventManager: HcmEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.deploymentService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'deploymentListModification',
        content: 'Deleted an deployment'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'hcm-deployment-delete-popup',
  template: ''
})
export class DeploymentDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ deployment }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(DeploymentDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.deployment = deployment;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/deployment', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/deployment', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
