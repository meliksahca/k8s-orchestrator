import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDeployment } from 'app/shared/model/deployment.model';

type EntityResponseType = HttpResponse<IDeployment>;
type EntityArrayResponseType = HttpResponse<IDeployment[]>;

@Injectable({ providedIn: 'root' })
export class DeploymentService {
  public resourceUrl = SERVER_API_URL + 'api/deployments';

  constructor(protected http: HttpClient) {}

  create(deployment: IDeployment): Observable<EntityResponseType> {
    return this.http.post<IDeployment>(this.resourceUrl, deployment, { observe: 'response' });
  }

  update(deployment: IDeployment): Observable<EntityResponseType> {
    return this.http.put<IDeployment>(this.resourceUrl, deployment, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDeployment>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDeployment[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
