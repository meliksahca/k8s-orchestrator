import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { HcmEventManager, HcmAlertService } from 'app/framework/public_api';

import { IDeployment } from 'app/shared/model/deployment.model';
import { AccountService } from 'app/core';
import { DeploymentService } from './deployment.service';

@Component({
  selector: 'hcm-deployment',
  templateUrl: './deployment.component.html'
})
export class DeploymentComponent implements OnInit, OnDestroy {
  deployments: IDeployment[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected deploymentService: DeploymentService,
    protected hcmAlertService: HcmAlertService,
    protected eventManager: HcmEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.deploymentService
      .query()
      .pipe(
        filter((res: HttpResponse<IDeployment[]>) => res.ok),
        map((res: HttpResponse<IDeployment[]>) => res.body)
      )
      .subscribe(
        (res: IDeployment[]) => {
          this.deployments = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInDeployments();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IDeployment) {
    return item.id;
  }

  registerChangeInDeployments() {
    this.eventSubscriber = this.eventManager.subscribe('deploymentListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.hcmAlertService.error(errorMessage, null, null);
  }
}
