import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'namespace',
        loadChildren: () => import('./namespace/namespace.module').then(m => m.K8SOrchestratorNamespaceModule)
      },
      {
        path: 'deployment',
        loadChildren: () => import('./deployment/deployment.module').then(m => m.K8SOrchestratorDeploymentModule)
      },
      {
        path: 'container',
        loadChildren: () => import('./container/container.module').then(m => m.K8SOrchestratorContainerModule)
      },
      {
        path: 'port',
        loadChildren: () => import('./port/port.module').then(m => m.K8SOrchestratorPortModule)
      }
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class K8SOrchestratorEntityModule {}
