import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { HcmEventManager, HcmAlertService } from 'app/framework/public_api';

import { IContainer } from 'app/shared/model/container.model';
import { AccountService } from 'app/core';
import { ContainerService } from './container.service';

@Component({
  selector: 'hcm-container',
  templateUrl: './container.component.html'
})
export class ContainerComponent implements OnInit, OnDestroy {
  containers: IContainer[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected containerService: ContainerService,
    protected hcmAlertService: HcmAlertService,
    protected eventManager: HcmEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.containerService
      .query()
      .pipe(
        filter((res: HttpResponse<IContainer[]>) => res.ok),
        map((res: HttpResponse<IContainer[]>) => res.body)
      )
      .subscribe(
        (res: IContainer[]) => {
          this.containers = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInContainers();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IContainer) {
    return item.id;
  }

  registerChangeInContainers() {
    this.eventSubscriber = this.eventManager.subscribe('containerListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.hcmAlertService.error(errorMessage, null, null);
  }
}
