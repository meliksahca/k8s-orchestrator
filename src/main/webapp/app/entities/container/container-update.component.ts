import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { HcmAlertService } from 'app/framework/public_api';
import { IContainer, Container } from 'app/shared/model/container.model';
import { ContainerService } from './container.service';
import { IDeployment } from 'app/shared/model/deployment.model';
import { DeploymentService } from 'app/entities/deployment';

@Component({
  selector: 'hcm-container-update',
  templateUrl: './container-update.component.html'
})
export class ContainerUpdateComponent implements OnInit {
  isSaving: boolean;

  deployments: IDeployment[];

  editForm = this.fb.group({
    id: [],
    image: [],
    name: [],
    deployment: []
  });

  constructor(
    protected hcmAlertService: HcmAlertService,
    protected containerService: ContainerService,
    protected deploymentService: DeploymentService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ container }) => {
      this.updateForm(container);
    });
    this.deploymentService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IDeployment[]>) => mayBeOk.ok),
        map((response: HttpResponse<IDeployment[]>) => response.body)
      )
      .subscribe((res: IDeployment[]) => (this.deployments = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(container: IContainer) {
    this.editForm.patchValue({
      id: container.id,
      image: container.image,
      name: container.name,
      deployment: container.deployment
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const container = this.createFromForm();
    if (container.id !== undefined) {
      this.subscribeToSaveResponse(this.containerService.update(container));
    } else {
      this.subscribeToSaveResponse(this.containerService.create(container));
    }
  }

  private createFromForm(): IContainer {
    return {
      ...new Container(),
      id: this.editForm.get(['id']).value,
      image: this.editForm.get(['image']).value,
      name: this.editForm.get(['name']).value,
      deployment: this.editForm.get(['deployment']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IContainer>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.hcmAlertService.error(errorMessage, null, null);
  }

  trackDeploymentById(index: number, item: IDeployment) {
    return item.id;
  }
}
