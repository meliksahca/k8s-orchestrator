import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { HcmAlertService } from 'app/framework/public_api';
import { IPort, Port } from 'app/shared/model/port.model';
import { PortService } from './port.service';
import { IContainer } from 'app/shared/model/container.model';
import { ContainerService } from 'app/entities/container';

@Component({
  selector: 'hcm-port-update',
  templateUrl: './port-update.component.html'
})
export class PortUpdateComponent implements OnInit {
  isSaving: boolean;

  containers: IContainer[];

  editForm = this.fb.group({
    id: [],
    port: [],
    container: []
  });

  constructor(
    protected hcmAlertService: HcmAlertService,
    protected portService: PortService,
    protected containerService: ContainerService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ port }) => {
      this.updateForm(port);
    });
    this.containerService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IContainer[]>) => mayBeOk.ok),
        map((response: HttpResponse<IContainer[]>) => response.body)
      )
      .subscribe((res: IContainer[]) => (this.containers = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(port: IPort) {
    this.editForm.patchValue({
      id: port.id,
      port: port.port,
      container: port.container
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const port = this.createFromForm();
    if (port.id !== undefined) {
      this.subscribeToSaveResponse(this.portService.update(port));
    } else {
      this.subscribeToSaveResponse(this.portService.create(port));
    }
  }

  private createFromForm(): IPort {
    return {
      ...new Port(),
      id: this.editForm.get(['id']).value,
      port: this.editForm.get(['port']).value,
      container: this.editForm.get(['container']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPort>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.hcmAlertService.error(errorMessage, null, null);
  }

  trackContainerById(index: number, item: IContainer) {
    return item.id;
  }
}
