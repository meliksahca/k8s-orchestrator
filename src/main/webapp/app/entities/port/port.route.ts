import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Port } from 'app/shared/model/port.model';
import { PortService } from './port.service';
import { PortComponent } from './port.component';
import { PortDetailComponent } from './port-detail.component';
import { PortUpdateComponent } from './port-update.component';
import { PortDeletePopupComponent } from './port-delete-dialog.component';
import { IPort } from 'app/shared/model/port.model';

@Injectable({ providedIn: 'root' })
export class PortResolve implements Resolve<IPort> {
  constructor(private service: PortService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPort> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Port>) => response.ok),
        map((port: HttpResponse<Port>) => port.body)
      );
    }
    return of(new Port());
  }
}

export const portRoute: Routes = [
  {
    path: '',
    component: PortComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Ports'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PortDetailComponent,
    resolve: {
      port: PortResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Ports'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PortUpdateComponent,
    resolve: {
      port: PortResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Ports'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PortUpdateComponent,
    resolve: {
      port: PortResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Ports'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const portPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: PortDeletePopupComponent,
    resolve: {
      port: PortResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Ports'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
