import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { HcmEventManager, HcmAlertService } from 'app/framework/public_api';

import { IPort } from 'app/shared/model/port.model';
import { AccountService } from 'app/core';
import { PortService } from './port.service';

@Component({
  selector: 'hcm-port',
  templateUrl: './port.component.html'
})
export class PortComponent implements OnInit, OnDestroy {
  ports: IPort[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected portService: PortService,
    protected hcmAlertService: HcmAlertService,
    protected eventManager: HcmEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.portService
      .query()
      .pipe(
        filter((res: HttpResponse<IPort[]>) => res.ok),
        map((res: HttpResponse<IPort[]>) => res.body)
      )
      .subscribe(
        (res: IPort[]) => {
          this.ports = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInPorts();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IPort) {
    return item.id;
  }

  registerChangeInPorts() {
    this.eventSubscriber = this.eventManager.subscribe('portListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.hcmAlertService.error(errorMessage, null, null);
  }
}
