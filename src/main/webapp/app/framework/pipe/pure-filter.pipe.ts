import { Pipe, PipeTransform } from '@angular/core';
import { HcmFilterPipe } from './filter.pipe';

@Pipe({ name: 'pureFilter' })
export class HcmPureFilterPipe extends HcmFilterPipe implements PipeTransform {
  transform(input: any[], filter: any, field?: string): any {
    return super.transform(input, filter, field);
  }
}
