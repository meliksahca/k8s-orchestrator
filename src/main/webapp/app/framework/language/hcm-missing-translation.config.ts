import { MissingTranslationHandler, MissingTranslationHandlerParams } from '@ngx-translate/core';
import { HcmConfigService } from '../config.service';

export class HcmMissingTranslationHandler implements MissingTranslationHandler {
  constructor(private configService: HcmConfigService) {}

  handle(params: MissingTranslationHandlerParams) {
    const key = params.key;
    return `${this.configService.getConfig().noi18nMessage}[${key}]`;
  }
}
