import { Input, Directive, ElementRef, OnChanges, OnInit, Optional } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { HcmConfigService } from '../config.service';

/**
 * A wrapper directive on top of the translate pipe as the inbuilt translate directive from ngx-translate is too verbose and buggy
 */
@Directive({
  selector: '[hcmTranslate]'
})
export class HcmTranslateDirective implements OnChanges, OnInit {
  @Input() hcmTranslate: string;
  @Input() translateValues: any;

  constructor(private configService: HcmConfigService, private el: ElementRef, @Optional() private translateService: TranslateService) {}

  ngOnInit() {
    const enabled = this.configService.getConfig().i18nEnabled;
    if (enabled) {
      this.translateService.onLangChange.subscribe(() => {
        this.getTranslation();
      });
    }
  }

  ngOnChanges() {
    const enabled = this.configService.getConfig().i18nEnabled;

    if (enabled) {
      this.getTranslation();
    }
  }

  private getTranslation() {
    this.translateService.get(this.hcmTranslate, this.translateValues).subscribe(
      value => {
        this.el.nativeElement.innerHTML = value;
      },
      () => {
        return `${this.configService.getConfig().noi18nMessage}[${this.hcmTranslate}]`;
      }
    );
  }
}
