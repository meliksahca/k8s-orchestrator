import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { HcmConfigService } from '../config.service';

@Injectable({
  providedIn: 'root'
})
export class HcmLanguageService {
  currentLang = 'en';

  constructor(private translateService: TranslateService, private configService: HcmConfigService) {
    this.init();
  }

  init() {
    const config = this.configService.getConfig();
    this.currentLang = config.defaultI18nLang;
    this.translateService.setDefaultLang(this.currentLang);
    this.translateService.use(this.currentLang);
  }

  changeLanguage(languageKey: string) {
    this.currentLang = languageKey;
    this.configService.CONFIG_OPTIONS.defaultI18nLang = languageKey;
    this.translateService.use(this.currentLang);
  }

  getCurrent(): Promise<string> {
    return Promise.resolve(this.currentLang);
  }
}
