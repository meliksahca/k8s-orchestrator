import { Directive, forwardRef, Input } from '@angular/core';
import { FormControl, NG_VALIDATORS } from '@angular/forms';

import { numberOfBytes } from './number-of-bytes';

@Directive({
  selector: '[hcmMinbytes][ngModel]',
  // eslint-disable-next-line @typescript-eslint/no-use-before-define
  providers: [{ provide: NG_VALIDATORS, useExisting: forwardRef(() => HcmMinbytesValidatorDirective), multi: true }]
})
export class HcmMinbytesValidatorDirective {
  @Input() hcmMinbytes: number;

  constructor() {}

  validate(c: FormControl) {
    return c.value && numberOfBytes(c.value) < this.hcmMinbytes
      ? {
          minbytes: {
            valid: false
          }
        }
      : null;
  }
}
