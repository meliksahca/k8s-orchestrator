import { AfterContentInit, ContentChild, Directive, Host, HostListener, Input } from '@angular/core';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';

import { HcmConfigService } from '../config.service';
import { HcmSortDirective } from './sort.directive';

@Directive({
  selector: '[hcmSortBy]'
})
export class HcmSortByDirective implements AfterContentInit {
  @Input() hcmSortBy: string;
  @ContentChild(FaIconComponent, { static: true }) iconComponent: FaIconComponent;

  sortIcon: IconDefinition;
  sortAscIcon: IconDefinition;
  sortDescIcon: IconDefinition;

  constructor(@Host() private hcmSort: HcmSortDirective, configService: HcmConfigService) {
    this.hcmSort = hcmSort;
    const config = configService.getConfig();
    this.sortIcon = config.sortIcon;
    this.sortAscIcon = config.sortAscIcon;
    this.sortDescIcon = config.sortDescIcon;
  }

  ngAfterContentInit(): void {
    if (this.hcmSort.predicate && this.hcmSort.predicate !== '_score' && this.hcmSort.predicate === this.hcmSortBy) {
      this.updateIconDefinition(this.iconComponent, this.hcmSort.ascending ? this.sortDescIcon : this.sortAscIcon);
      this.hcmSort.activeIconComponent = this.iconComponent;
    }
  }

  @HostListener('click')
  onClick() {
    if (this.hcmSort.predicate && this.hcmSort.predicate !== '_score') {
      this.hcmSort.sort(this.hcmSortBy);
      this.updateIconDefinition(this.hcmSort.activeIconComponent, this.sortIcon);
      this.updateIconDefinition(this.iconComponent, this.hcmSort.ascending ? this.sortDescIcon : this.sortAscIcon);
      this.hcmSort.activeIconComponent = this.iconComponent;
    }
  }

  private updateIconDefinition(iconComponent: FaIconComponent, icon: IconDefinition) {
    if (iconComponent) {
      iconComponent.iconProp = icon;
      iconComponent.ngOnChanges({});
    }
  }
}
