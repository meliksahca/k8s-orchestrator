import { Directive, forwardRef, Input } from '@angular/core';
import { FormControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[hcmMin][ngModel]',
  // eslint-disable-next-line @typescript-eslint/no-use-before-define
  providers: [{ provide: NG_VALIDATORS, useExisting: forwardRef(() => HcmMinValidatorDirective), multi: true }]
})
export class HcmMinValidatorDirective {
  @Input() hcmMin: number;

  constructor() {}

  validate(c: FormControl) {
    return c.value === undefined || c.value === null || c.value >= this.hcmMin
      ? null
      : {
          min: {
            valid: false
          }
        };
  }
}
