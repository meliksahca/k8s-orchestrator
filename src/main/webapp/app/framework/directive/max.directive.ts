import { Directive, forwardRef, Input } from '@angular/core';
import { FormControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[hcmMax][ngModel]',
  // eslint-disable-next-line @typescript-eslint/no-use-before-define
  providers: [{ provide: NG_VALIDATORS, useExisting: forwardRef(() => HcmMaxValidatorDirective), multi: true }]
})
export class HcmMaxValidatorDirective {
  @Input() hcmMax: number;

  constructor() {}

  validate(c: FormControl) {
    return c.value === undefined || c.value === null || c.value <= this.hcmMax
      ? null
      : {
          max: {
            valid: false
          }
        };
  }
}
