import { Directive, forwardRef, Input } from '@angular/core';
import { FormControl, NG_VALIDATORS } from '@angular/forms';

import { numberOfBytes } from './number-of-bytes';

@Directive({
  selector: '[hcmMaxbytes][ngModel]',
  // eslint-disable-next-line @typescript-eslint/no-use-before-define
  providers: [{ provide: NG_VALIDATORS, useExisting: forwardRef(() => HcmMaxbytesValidatorDirective), multi: true }]
})
export class HcmMaxbytesValidatorDirective {
  @Input() hcmMaxbytes: number;

  constructor() {}

  validate(c: FormControl) {
    return c.value && numberOfBytes(c.value) > this.hcmMaxbytes
      ? {
          maxbytes: {
            valid: false
          }
        }
      : null;
  }
}
