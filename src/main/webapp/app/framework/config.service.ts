import { Injectable } from '@angular/core';
import { HcmModuleConfig } from './config';

@Injectable({
  providedIn: 'root'
})
export class HcmConfigService {
  CONFIG_OPTIONS: HcmModuleConfig;

  constructor(moduleConfig?: HcmModuleConfig) {
    this.CONFIG_OPTIONS = {
      ...new HcmModuleConfig(),
      ...moduleConfig
    };
  }

  getConfig(): HcmModuleConfig {
    return this.CONFIG_OPTIONS;
  }
}
