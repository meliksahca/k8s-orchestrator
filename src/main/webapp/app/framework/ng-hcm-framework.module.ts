import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HcmThreadModalComponent } from './component/metrics/hcm-metrics-modal-threads.component';
import { HcmModuleConfig } from './config';
import { HcmConfigService } from './config.service';
import { HCM_COMPONENTS, HCM_DIRECTIVES, HCM_PIPES } from './hcm-components';
import { HcmMissingTranslationHandler } from './language/hcm-missing-translation.config';
import { HcmTranslateDirective } from './language/hcm-translate.directive';

export function translatePartialLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'i18n/', `.json?buildTimestamp=${process.env.BUILD_TIMESTAMP}`);
}

export function missingTranslationHandler(configService: HcmConfigService) {
  return new HcmMissingTranslationHandler(configService);
}

@NgModule({
  imports: [CommonModule, NgbModule, FormsModule],
  declarations: [...HCM_PIPES, ...HCM_DIRECTIVES, ...HCM_COMPONENTS, HcmTranslateDirective],
  entryComponents: [HcmThreadModalComponent],
  exports: [...HCM_PIPES, ...HCM_DIRECTIVES, ...HCM_COMPONENTS, HcmTranslateDirective, CommonModule]
})
export class NgHcmFrameworkModule {
  static forRoot(moduleConfig: HcmModuleConfig): ModuleWithProviders {
    return {
      ngModule: NgHcmFrameworkModule,
      providers: [{ provide: HcmModuleConfig, useValue: moduleConfig }]
    };
  }
}
