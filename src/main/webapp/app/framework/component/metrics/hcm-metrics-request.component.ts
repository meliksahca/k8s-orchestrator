import { Component, Input } from '@angular/core';

@Component({
  selector: 'hcm-metrics-request',
  template: `
    <h3 hcmTranslate="metrics.jvm.http.title">HTTP requests (time in millisecond)</h3>
    <table class="table table-striped" *ngIf="!updating">
      <thead>
        <tr>
          <th hcmTranslate="metrics.jvm.http.table.code">Code</th>
          <th hcmTranslate="metrics.jvm.http.table.count">Count</th>
          <th class="text-right" hcmTranslate="metrics.jvm.http.table.mean">Mean</th>
          <th class="text-right" hcmTranslate="metrics.jvm.http.table.max">Max</th>
        </tr>
      </thead>
      <tbody>
        <tr *ngFor="let entry of requestMetrics['percode'] | keys">
          <td>{{ entry.key }}</td>
          <td>
            <ngb-progressbar
              [max]="requestMetrics['all'].count"
              [value]="entry.value.count"
              [striped]="true"
              [animated]="false"
              type="success"
            >
              <span>{{ entry.value.count }}</span>
            </ngb-progressbar>
          </td>
          <td class="text-right">
            {{ filterNaN(entry.value.mean) | number: '1.0-2' }}
          </td>
          <td class="text-right">{{ entry.value.max | number: '1.0-2' }}</td>
        </tr>
      </tbody>
    </table>
  `
})
export class HcmMetricsHttpRequestComponent {
  /**
   * object containing http request related metrics
   */
  @Input() requestMetrics: {};

  /**
   * boolean field saying if the metrics are in the process of being updated
   */
  @Input() updating: boolean;

  filterNaN(input) {
    if (isNaN(input)) {
      return 0;
    }
    return input;
  }
}
