import { Component, Input } from '@angular/core';

@Component({
  selector: 'hcm-metrics-datasource',
  template: `
    <h3 hcmTranslate="metrics.datasource.title">DataSource statistics (time in millisecond)</h3>
    <div class="table-responsive" *ngIf="!updating">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>
              <span hcmTranslate="metrics.datasource.usage">Connection Pool Usage</span> (active: {{ datasourceMetrics.active.value }}, min:
              {{ datasourceMetrics.min.value }}, max: {{ datasourceMetrics.max.value }}, idle: {{ datasourceMetrics.idle.value }})
            </th>
            <th class="text-right" hcmTranslate="metrics.datasource.count">Count</th>
            <th class="text-right" hcmTranslate="metrics.datasource.mean">Mean</th>
            <th class="text-right" hcmTranslate="metrics.servicesstats.table.min">Min</th>
            <th class="text-right" hcmTranslate="metrics.servicesstats.table.p50">p50</th>
            <th class="text-right" hcmTranslate="metrics.servicesstats.table.p75">p75</th>
            <th class="text-right" hcmTranslate="metrics.servicesstats.table.p95">p95</th>
            <th class="text-right" hcmTranslate="metrics.servicesstats.table.p99">p99</th>
            <th class="text-right" hcmTranslate="metrics.datasource.max">Max</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Acquire</td>
            <td class="text-right">{{ datasourceMetrics.acquire.count }}</td>
            <td class="text-right">{{ filterNaN(datasourceMetrics.acquire.mean) | number: '1.0-2' }}</td>
            <td class="text-right">{{ datasourceMetrics.acquire['0.0'] | number: '1.0-3' }}</td>
            <td class="text-right">{{ datasourceMetrics.acquire['0.5'] | number: '1.0-3' }}</td>
            <td class="text-right">{{ datasourceMetrics.acquire['0.75'] | number: '1.0-3' }}</td>
            <td class="text-right">{{ datasourceMetrics.acquire['0.95'] | number: '1.0-3' }}</td>
            <td class="text-right">{{ datasourceMetrics.acquire['0.99'] | number: '1.0-3' }}</td>
            <td class="text-right">{{ filterNaN(datasourceMetrics.acquire.max) | number: '1.0-2' }}</td>
          </tr>
          <tr>
            <td>Creation</td>
            <td class="text-right">{{ datasourceMetrics.creation.count }}</td>
            <td class="text-right">{{ filterNaN(datasourceMetrics.creation.mean) | number: '1.0-2' }}</td>
            <td class="text-right">{{ datasourceMetrics.creation['0.0'] | number: '1.0-3' }}</td>
            <td class="text-right">{{ datasourceMetrics.creation['0.5'] | number: '1.0-3' }}</td>
            <td class="text-right">{{ datasourceMetrics.creation['0.75'] | number: '1.0-3' }}</td>
            <td class="text-right">{{ datasourceMetrics.creation['0.95'] | number: '1.0-3' }}</td>
            <td class="text-right">{{ datasourceMetrics.creation['0.99'] | number: '1.0-3' }}</td>
            <td class="text-right">{{ filterNaN(datasourceMetrics.creation.max) | number: '1.0-2' }}</td>
          </tr>
          <tr>
            <td>Usage</td>
            <td class="text-right">{{ datasourceMetrics.usage.count }}</td>
            <td class="text-right">{{ filterNaN(datasourceMetrics.usage.mean) | number: '1.0-2' }}</td>
            <td class="text-right">{{ datasourceMetrics.usage['0.0'] | number: '1.0-3' }}</td>
            <td class="text-right">{{ datasourceMetrics.usage['0.5'] | number: '1.0-3' }}</td>
            <td class="text-right">{{ datasourceMetrics.usage['0.75'] | number: '1.0-3' }}</td>
            <td class="text-right">{{ datasourceMetrics.usage['0.95'] | number: '1.0-3' }}</td>
            <td class="text-right">{{ datasourceMetrics.usage['0.99'] | number: '1.0-3' }}</td>
            <td class="text-right">{{ filterNaN(datasourceMetrics.usage.max) | number: '1.0-2' }}</td>
          </tr>
        </tbody>
      </table>
    </div>
  `
})
export class HcmMetricsDatasourceComponent {
  /**
   * object containing all datasource related metrics
   */
  @Input() datasourceMetrics: {
    active: any;
    min: any;
    idle: any;
    max: any;
    usage: any;
    acquire: any;
    creation: any;
  };

  /**
   * boolean field saying if the metrics are in the process of being updated
   */
  @Input() updating: boolean;

  filterNaN(input) {
    if (isNaN(input)) {
      return 0;
    }
    return input;
  }
}
