import { Component, Input } from '@angular/core';
import { HcmConfigService } from '../config.service';

/**
 * A component that will take care of item count statistics of a pagination.
 */
@Component({
  selector: 'hcm-item-count',
  template: `
    <div *ngIf="i18nEnabled; else noI18n" class="info hcm-item-count" hcmTranslate="global.item-count" [translateValues]="i18nValues()">
      /* [attr.translateValues] is used to get entire values in tests */
    </div>
    <ng-template #noI18n class="info hcm-item-count">
      Showing
      {{ (page - 1) * itemsPerPage == 0 ? 1 : (page - 1) * itemsPerPage + 1 }}
      - {{ page * itemsPerPage < total ? page * itemsPerPage : total }} of {{ total }} items.
    </ng-template>
  `
})
export class HcmItemCountComponent {
  /**
   *  current page number.
   */
  @Input() page: number;

  /**
   *  Total number of items.
   */
  @Input() total: number;

  /**
   *  Number of items per page.
   */
  @Input() itemsPerPage: number;

  /**
   * True if the generated application use i18n
   */
  i18nEnabled: boolean;

  constructor(config: HcmConfigService) {
    this.i18nEnabled = config.CONFIG_OPTIONS.i18nEnabled;
  }

  /**
   * "translate-values" JSON of the template
   */
  i18nValues(): Object {
    const first = (this.page - 1) * this.itemsPerPage === 0 ? 1 : (this.page - 1) * this.itemsPerPage + 1;
    const second = this.page * this.itemsPerPage < this.total ? this.page * this.itemsPerPage : this.total;

    return {
      first,
      second,
      total: this.total
    };
  }
}
