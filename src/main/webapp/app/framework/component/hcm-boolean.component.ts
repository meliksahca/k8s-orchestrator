import { Component, Input, OnInit } from '@angular/core';
import { HcmModuleConfig } from '../config';
import { HcmConfigService } from '../config.service';

/**
 * This component can be used to display a boolean value by defining the @Input attributes
 * If an attribute is not provided, default values will be applied (see HcmModuleConfig class)
 * Have a look at the following examples
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * <hcm-boolean [value]="inputBooleanVariable"></hcm-boolean>
 *
 * - Display a green check when inputBooleanVariable is true
 * - Display a red cross when inputBooleanVariable is false
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * <hcm-boolean
 *     [value]="inputBooleanVariable">
 *     classTrue="fa fa-lg fa-check text-primary"
 *     classFalse="fa fa-lg fa-times text-warning"
 * </hcm-boolean>
 *
 * - Display a blue check when inputBooleanVariable is true
 * - Display an orange cross when inputBooleanVariable is false
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * <hcm-boolean
 *     [value]="inputBooleanVariable">
 *     classTrue="fa fa-lg fa-check"
 *     classFalse=""
 * </hcm-boolean>
 *
 * - Display a black check when inputBooleanVariable is true
 * - Do not display anything when inputBooleanVariable is false
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * <hcm-boolean
 *     [value]="inputBooleanVariable"
 *     [textTrue]="'userManagement.activated' | translate"
 *     textFalse="deactivated">
 * </hcm-boolean>
 *
 * - Display a green badge when inputBooleanVariable is true
 * - Display a red badge when inputBooleanVariable is false
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * <hcm-boolean
 *     [value]="user.activated"
 *     classTrue="badge badge-warning"
 *     classFalse="badge badge-info"
 *     [textTrue]="'userManagement.activated' | translate"
 *     textFalse="deactivated">
 * </hcm-boolean>
 *
 * - Display an orange badge and write 'activated' when inputBooleanVariable is true
 * - Display a blue badge and write 'deactivated' when inputBooleanVariable is false
 */
@Component({
  selector: 'hcm-boolean',
  template: `
    <span [ngClass]="value ? classTrue : classFalse" [innerHtml]="value ? textTrue : textFalse"> </span>
  `
})
export class HcmBooleanComponent implements OnInit {
  /**
   * the boolean input value
   */
  @Input() value: boolean;

  /**
   * the class(es) (space separated) that will be applied if value is true
   */
  @Input() classTrue: string;

  /**
   * the class(es) (space separated) that will be applied if the input value is false
   */
  @Input() classFalse: string;

  /**
   * the text that will be displayed if the input value is true
   */
  @Input() textTrue: string;

  /**
   * the text that will be displayed if the input value is false
   */
  @Input() textFalse: string;

  config: HcmModuleConfig;

  constructor(configService: HcmConfigService) {
    this.config = configService.getConfig();
  }

  ngOnInit() {
    if (this.textTrue === undefined) {
      if (this.classTrue === undefined) {
        this.classTrue = this.config.classTrue;
      }
    } else {
      if (this.classTrue === undefined) {
        this.classTrue = this.config.classBadgeTrue;
      }
    }

    if (this.textFalse === undefined) {
      if (this.classFalse === undefined) {
        this.classFalse = this.config.classFalse;
      }
    } else {
      if (this.classFalse === undefined) {
        this.classFalse = this.config.classBadgeFalse;
      }
    }
  }
}
