import { HcmBooleanComponent } from './component/hcm-boolean.component';
import { HcmItemCountComponent } from './component/hcm-item-count.component';
import { HcmJvmMemoryComponent } from './component/metrics/hcm-jvm-memory.component';
import { HcmJvmThreadsComponent } from './component/metrics/hcm-jvm-threads.component';
import { HcmMetricsCacheComponent } from './component/metrics/hcm-metrics-cache.component';
import { HcmMetricsDatasourceComponent } from './component/metrics/hcm-metrics-datasource.component';
import { HcmMetricsEndpointsRequestsComponent } from './component/metrics/hcm-metrics-endpoints-requests';
import { HcmMetricsGarbageCollectorComponent } from './component/metrics/hcm-metrics-garbagecollector.component';
import { HcmThreadModalComponent } from './component/metrics/hcm-metrics-modal-threads.component';
import { HcmMetricsHttpRequestComponent } from './component/metrics/hcm-metrics-request.component';
import { HcmMetricsSystemComponent } from './component/metrics/hcm-metrics-system.component';
import { HcmMaxValidatorDirective } from './directive/max.directive';
import { HcmMaxbytesValidatorDirective } from './directive/maxbytes.directive';
import { HcmMinValidatorDirective } from './directive/min.directive';
import { HcmMinbytesValidatorDirective } from './directive/minbytes.directive';
import { HcmSortByDirective } from './directive/sort-by.directive';
import { HcmSortDirective } from './directive/sort.directive';
import { HcmCapitalizePipe } from './pipe/capitalize.pipe';
import { HcmFilterPipe } from './pipe/filter.pipe';
import { HcmKeysPipe } from './pipe/keys.pipe';
import { HcmOrderByPipe } from './pipe/order-by.pipe';
import { HcmPureFilterPipe } from './pipe/pure-filter.pipe';
import { HcmTruncateCharactersPipe } from './pipe/truncate-characters.pipe';
import { HcmTruncateWordsPipe } from './pipe/truncate-words.pipe';

export const HCM_PIPES = [
  HcmCapitalizePipe,
  HcmFilterPipe,
  HcmKeysPipe,
  HcmOrderByPipe,
  HcmPureFilterPipe,
  HcmTruncateCharactersPipe,
  HcmTruncateWordsPipe
];

export const HCM_DIRECTIVES = [
  HcmMaxValidatorDirective,
  HcmMinValidatorDirective,
  HcmMaxbytesValidatorDirective,
  HcmMinbytesValidatorDirective,
  HcmSortDirective,
  HcmSortByDirective
];

export const HCM_COMPONENTS = [
  HcmItemCountComponent,
  HcmBooleanComponent,
  HcmJvmMemoryComponent,
  HcmJvmThreadsComponent,
  HcmMetricsHttpRequestComponent,
  HcmMetricsEndpointsRequestsComponent,
  HcmMetricsCacheComponent,
  HcmMetricsDatasourceComponent,
  HcmMetricsSystemComponent,
  HcmMetricsGarbageCollectorComponent,
  HcmThreadModalComponent
];
