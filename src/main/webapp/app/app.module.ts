import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { NgHcmFrameworkModule } from './framework/public_api';

import { AuthInterceptor } from './blocks/interceptor/auth.interceptor';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { K8SOrchestratorSharedModule } from 'app/shared';
import { K8SOrchestratorCoreModule } from 'app/core';
import { K8SOrchestratorAppRoutingModule } from './app-routing.module';
import { K8SOrchestratorHomeModule } from './home/home.module';
import { K8SOrchestratorAccountModule } from './account/account.module';
import { K8SOrchestratorEntityModule } from './entities/entity.module';
import * as moment from 'moment';

import { HcmMainComponent, NavbarComponent, FooterComponent, PageRibbonComponent, ErrorComponent } from './layouts';

@NgModule({
  imports: [
    BrowserModule,
    NgxWebstorageModule.forRoot({ prefix: 'hcm', separator: '-' }),
    NgHcmFrameworkModule.forRoot({
      // set below to true to make alerts look like toast
      alertAsToast: false,
      alertTimeout: 5000
    }),
    K8SOrchestratorSharedModule.forRoot(),
    K8SOrchestratorCoreModule,
    K8SOrchestratorHomeModule,
    K8SOrchestratorAccountModule,

    K8SOrchestratorEntityModule,
    K8SOrchestratorAppRoutingModule
  ],
  declarations: [HcmMainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthExpiredInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlerInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: NotificationInterceptor,
      multi: true
    }
  ],
  bootstrap: [HcmMainComponent]
})
export class K8SOrchestratorAppModule {
  constructor(private dpConfig: NgbDatepickerConfig) {
    this.dpConfig.minDate = { year: moment().year() - 100, month: 1, day: 1 };
  }
}
