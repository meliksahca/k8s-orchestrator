# k8sOrchestrator

This application made for Hazelcast Cloud assessment.
For running back-end and front-end just execute gradle wrapper command nothing more

    ./gradlew

For testing rest endpoints you can directly use swagger page which is under

    http://localhost:8080/admin/docs

Do not forget to login with credentials `admin` and `admin`. I know there are much more things to do ui side it is not finished yet.

Setting credentials for kubernetes done under `application.yml` file.

```yaml
spring:
  application:
    name: k8sOrchestrator
  cloud:
    kubernetes:
      client:
        master-url: https://35.239.130.121/
        o-auth-token: xxyyzz
        # client-cert-file: /Users/meliksah/.minikube/client.crt
        # client-key-file: /Users/meliksah/.minikube/client.key
        # ca-cert-file: /Users/meliksah/.minikube/ca.crt
        # o-auth-token:
```

Basically I used two fields, which are master-url and o-auth-token(it is bearer token actually :)) for connecting kubernetes as a client. Creating token and service accounts needs some `RBAC` knowledge. I added example container configuration to `ngingx.yaml` which I used for testing and my role binding file for default service account to `role_bind.yaml`

Here is the example rest to create deployment at both db and kubernetes

```json
{
  "apiVersion": "apps/v1",
  "containers": [
    {
      "image": "nginx:1.7.9",
      "name": "nginx",
      "ports": [
        {
          "port": "80"
        }
      ]
    }
  ],
  "kind": "Deployment",
  "labelKey": "app",
  "labelValue": "nginx",
  "metadataName": "nginx-deployment",
  "namespace": {
    "name": "default"
  },
  "replica": 3
}
```

## Development

Before you can build this project, you must install and configure the following dependencies on your machine:

1. [Node.js][]: We use Node to run a development web server and build the project.
   Depending on your system, you can install Node either from source or as a pre-packaged bundle.

After installing Node, you should be able to run the following command to install development tools.
You will only need to run this command when dependencies change in [package.json](package.json).

    npm install

We use npm scripts and [Webpack][] as our build system.

Run the following commands in two separate terminals to create a blissful development experience where your browser
auto-refreshes when files change on your hard drive.

    ./gradlew
    npm start

Npm is also used to manage CSS and JavaScript dependencies used in this application. You can upgrade dependencies by
specifying a newer version in [package.json](package.json). You can also run `npm update` and `npm install` to manage dependencies.
Add the `help` flag on any command to see how you can use it. For example, `npm help update`.

The `npm run` command will list all of the scripts available to run for this project.

### Using Angular CLI

You can also use [Angular CLI][] to generate some custom client code.

For example, the following command:

    ng generate component my-component

will generate few files:

    create src/main/webapp/app/my-component/my-component.component.html
    create src/main/webapp/app/my-component/my-component.component.ts
    update src/main/webapp/app/app.module.ts

## Building for production

### Packaging as jar

To build the final jar and optimize the k8sOrchestrator application for production, run:

    ./gradlew -Pprod clean bootJar

This will concatenate and minify the client CSS and JavaScript files. It will also modify `index.html` so it references these new files.
To ensure everything worked, run:

    java -jar build/libs/*.jar

Then navigate to [http://localhost:8080](http://localhost:8080) in your browser.

### Packaging as war

To package your application as a war in order to deploy it to an application server, run:

    ./gradlew -Pprod -Pwar clean bootWar

## Testing

To launch your application's tests, run:

    ./gradlew test integrationTest

### Client tests

Unit tests are run by [Jest][] and written with [Jasmine][]. They're located in [src/test/javascript/](src/test/javascript/) and can be run with:

    npm test

For more information, refer to the [Running tests page][].

### Code quality

Sonar is used to analyse code quality. You can start a local Sonar server (accessible on http://localhost:9001) with:

```
docker-compose -f src/main/docker/sonar.yml up -d
```

You can run a Sonar analysis with using the [sonar-scanner](https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner) or by using the gradle plugin.

Then, run a Sonar analysis:

```
./gradlew -Pprod clean check sonarqube
```

For more information, refer to the [Code quality page][].

## Using Docker to simplify development (optional)

For example, to start a mysql database in a docker container, run:

    docker-compose -f src/main/docker/mysql.yml up -d

To stop it and remove the container, run:

    docker-compose -f src/main/docker/mysql.yml down

You can also fully dockerize your application and all the services that it depends on.
To achieve this, first build a docker image of your app by running:

    ./gradlew bootJar -Pprod jibDockerBuild

Then run:

    docker-compose -f src/main/docker/app.yml up -d
